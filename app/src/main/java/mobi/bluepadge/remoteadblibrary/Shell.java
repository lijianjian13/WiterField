package mobi.bluepadge.remoteadblibrary;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mobi.bluepadge.ui.Settings;

//import java.io.File;

/**
 * Created by bluepadge on 16/12/8.
 */

public class Shell {
    private static final String TAG = Shell.class.getSimpleName();
    //    private static AdbConnection mAdbConnection;
//    private static AdbConnection mAdbConnection = null;
    private static AdbStream streamInput;
//    private static Socket sock = null;

    // This implements the AdbBase64 interface required for AdbCrypto
    public static AdbBase64 getBase64Impl() {
        return new AdbBase64() {
            @Override
            public String encodeToString(byte[] data) {
                return Base64.encodeToString(data, Base64.NO_WRAP);
            }
        };
    }

    // This function loads a keypair from the specified files if one exists, and if not,
    // it creates a new keypair and saves it in the specified files
    public static AdbCrypto setupCrypto(Context context, String pubKeyFile, String privKeyFile)
            throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        File pub = new File(context.getFilesDir(), pubKeyFile);
//        File pub = new File(pubKeyFile);
        File priv = new File(context.getFilesDir(), privKeyFile);
        AdbCrypto c = null;

        // Try to load a key pair from the files
        if (pub.exists() && priv.exists()) {
            try {
                c = AdbCrypto.loadAdbKeyPair(getBase64Impl(), priv, pub);
            } catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException e) {
                // Failed to read from file
                c = null;
            }
        }

        if (c == null) {
            // We couldn't load a key, so let's generate a new one
            c = AdbCrypto.generateAdbKeyPair(getBase64Impl());

            // Save it
            c.saveAdbKeyPair(priv, pub);
            Log.d(TAG, "Generated new keypair");
        } else {
            Log.d(TAG, "Loaded existing keypair");
        }

        return c;
    }

//    public static boolean isActive(Context context) {
//        return initAdb(context) != null && mAdbConnection.isConnected();
//    }

    public static List<String> run(Context context, String command) {
        List<String> commands = new ArrayList<>();
        commands.add(command);
        return run(context, commands);
    }

    public static synchronized List<String> run(Context context, List<String> commands) {
        final List<String> res = Collections.synchronizedList(new ArrayList<String>());

//        if (mAdbConnection == null || !mAdbConnection.isConnected()) {
        int port = Settings.instance().getInt(Settings.ADB_PORT, 5555);
        AdbConnection adbConnection = initAdb(context, port);
        if (adbConnection == null) return null;
//        }
        String target = "shell:";
        for (String command : commands) {
            target += command + ";";
        }

//        String openTarget = "shell:" + commands + ";" + "am force-stop me.ele";
        try {
            streamInput = adbConnection.open(target);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


        String tempOut = "";
        while (!streamInput.isClosed()) {
            try {
                String result = new String(streamInput.read(), "UTF-8");
//                Log.d("TEST", result);
                tempOut += result;

            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        try {
            adbConnection.close();
//            streamInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Log.d("ready return", tempOut);
        Collections.addAll(res, tempOut.split("\\r?\\n"));

        return res;
    }

    private static AdbConnection initAdb(final Context context,int port) {

        AdbConnection mAdbConnection;
        Socket sock = null;
//        if (mAdbConnection != null && mAdbConnection.isConnected()) {
//            return mAdbConnection;
//        }

//        if (crypto == null) {
        AdbCrypto crypto;
        // Setup the crypto object required for the AdbConnection
        try {
            crypto = setupCrypto(context, "pub.key", "priv.key");
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException e) {
            e.printStackTrace();
            return null;
        }
//        }

//        if (sock == null || !sock.isConnected()) {

        // Connect the socket to the remote host
        Log.d(TAG, "Socket connecting...");
        try {
            sock = new Socket("127.0.0.1", port);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG, "Socket connected");
//        }
        try {
            mAdbConnection = AdbConnection.create(sock, crypto);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG, "ADB connecting...");

        // Start the application layer connection process
        try {
            mAdbConnection.connect();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG, "ADB connected");

        return mAdbConnection;
    }
}
