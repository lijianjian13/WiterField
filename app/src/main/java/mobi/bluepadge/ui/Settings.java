package mobi.bluepadge.ui;

import android.content.Context;
import android.content.SharedPreferences;


public class Settings {

    public static final String SHOW_SYSTEM_APP = "show_system_app";
    public static final String SHOW_INTRO = "show_intro";
    public static final String ADB_PORT = "adb_port";
    public static final String TIMER = "timer";

    private static Settings sInstance;

    public static void init(Context context) {
        sInstance = new Settings(context, "settings");
    }

    public static Settings instance() {
        if (sInstance == null) {
            throw new RuntimeException("Call init in Application onCreate()");
        }

        return sInstance;
    }

    private SharedPreferences mPrefs;

    private Settings(Context context, String name) {
        mPrefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public Settings putBoolean(String key, boolean value) {
        mPrefs.edit()
                .putBoolean(key, value)
                .apply();

        return this;
    }

    public boolean getBoolean(String key, boolean def) {
        return mPrefs.getBoolean(key, def);
    }

    public Settings putInt(String key, int value) {
        mPrefs.edit()
                .putInt(key, value)
                .apply();

        return this;
    }

    public int getInt(String key, int defValue) {
        return mPrefs.getInt(key, defValue);
    }

    public Settings putLong(String key, long value) {
        mPrefs.edit()
                .putLong(key, value)
                .apply();

        return this;
    }

    public long getLong(String key, long defValue) {
        return mPrefs.getLong(key, defValue);
    }

    public Settings putString(String key, String value) {
        mPrefs.edit()
                .putString(key, value)
                .apply();

        return this;
    }

    public String getString(String key, String defValue) {
        return mPrefs.getString(key, defValue);
    }

    public Settings putIntToString(String key, int value) {
        mPrefs.edit()
                .putString(key, Integer.toString(value))
                .apply();

        return this;
    }

    public int getIntFromString(String key, int defValue) {
        return Integer.parseInt(mPrefs.getString(key, Integer.toString(defValue)));
    }
}
