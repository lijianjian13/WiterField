package mobi.bluepadge.ui;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobi.bluepadge.R;
import mobi.bluepadge.database.AppDataSource;
import mobi.bluepadge.database.model.AppInfo;
import mobi.bluepadge.database.model.BlockAppInfo;
import mobi.bluepadge.services.IdleService;
import mobi.bluepadge.ui.Bus.AppEvent;
import mobi.bluepadge.ui.Bus.RxBus;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends AppCompatActivity {
    private static final String ADB_INACTIVE_ERROR = "ADB_INACTIVE_ERROR";

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private AppSectionAdapter mAdapter;
    private AppDataSource mAppDataSource;
    private Toolbar mToolbar;
    private CompositeSubscription mCompositeSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCompositeSubscription = new CompositeSubscription();

        Subscription subscription = RxBus.getInstance().toObserverable(AppEvent.class)
                .subscribe(appEvent -> {
                    refresh(Settings.instance().getBoolean(Settings.SHOW_SYSTEM_APP, false));
                });
        mCompositeSubscription.add(subscription);

        mAppDataSource = new AppDataSource(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mProgressBar = (ProgressBar) findViewById(android.R.id.progress);

        mRecyclerView = (RecyclerView) findViewById(android.R.id.list);
        mAdapter = new AppSectionAdapter();
        mRecyclerView.setLayoutManager(new StickyHeaderLayoutManager());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        refresh(Settings.instance().getBoolean(Settings.SHOW_SYSTEM_APP, false));

        startService(new Intent(this, IdleService.class));

        /*
        JobScheduler jobScheduler =
                (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        int state = jobScheduler.schedule(new JobInfo.Builder(1000,
                new ComponentName(this, IdleAppJobService.class))
                .setRequiresDeviceIdle(true)
                .build());
        Log.d("MainActivity", "schedule result: " + (state == JobScheduler.RESULT_SUCCESS));
        */
    }

    private void refresh(final boolean showSystemPackage) {
        mRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        Observable<List<AppInfo>> appObservable =
                Observable.create(new Observable.OnSubscribe<List<AppInfo>>() {
                    @Override
                    public void call(Subscriber<? super List<AppInfo>> subscriber) {
                        final List<AppInfo> list = new ArrayList<>();
                        for (PackageInfo pi : getPackageManager().getInstalledPackages(0)) {
                            try {
                                boolean isSystem =
                                        (getPackageManager().getApplicationInfo(pi.packageName, 0).flags
                                                & ApplicationInfo.FLAG_SYSTEM) != 0;
                                if (showSystemPackage || !isSystem) {
                                    list.add(new AppInfo(pi, getPackageManager()));
                                }
                            } catch (PackageManager.NameNotFoundException ignored) {
                            }
                        }

//                        Collections.sort(list, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                        Collections.sort(list, (o1, o2) -> o2.getInstallTime().compareTo(o1.getInstallTime()));

                        subscriber.onNext(list);

                        subscriber.onCompleted();
                    }
                });

        Observable<Map<String, ActivityManager.RunningServiceInfo>> appRunningObservable =
                Observable.create(subscriber -> {
//                    final List<AppInfo> list = new ArrayList<AppInfo>();
//                    final Set<String> list = new HashSet<String>();
                    final Map<String, ActivityManager.RunningServiceInfo> runningMap = new HashMap<>();
                    ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                    List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
//                    for (ActivityManager.RunningAppProcessInfo runningApp : runningApps) {
//                        list.add(runningApp.processName);
//                    }
//                    try {
//                    am.getRunningAppProcesses()
                    List<ActivityManager.RunningServiceInfo> runningServices = am.getRunningServices(Integer.MAX_VALUE);
                    for (ActivityManager.RunningServiceInfo runningService : runningServices) {
//                            list.add(new AppInfo(runningAppInfo.processName, getPackageManager()));
//                        if (runningService.flags != 0) {
                        String packageName = runningService.service.getPackageName();
//                        list.add(runningService.service.getPackageName());
                        runningMap.put(packageName, runningService);
                        Log.d("Running", packageName + " flags:" + runningService.flags);
//                        }
//                        runningService.
                    }
//                    } catch (PackageManager.NameNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    Collections.sort(list, (o1, o2) -> o2.getInstallTime().compareTo(o1.getInstallTime()));

                    subscriber.onNext(runningMap);

                    subscriber.onCompleted();
                });


        Observable<List<BlockAppInfo>> appBlockObservable = mAppDataSource.getBlockApps();

        Subscription subscription =
                Observable.zip(appObservable, appBlockObservable,
                        (appInfoList, appBlockInfoList) -> {
                            for (AppInfo appInfo : appInfoList) {
//                                if (appRunningInfoMap.containsKey(appInfo.getPackageName())) {
//                                    Log.d("Running:", appInfo.getPackageName());
//                                    appInfo.setRunning(true);
//                                    appInfo.setRunningInfo(appRunningInfoMap.get(appInfo.getPackageName()));
//                                }
                                for (BlockAppInfo blockApp : appBlockInfoList) {
                                    if (TextUtils.equals(appInfo.getPackageName(), blockApp.getPackageName())) {
                                        appInfo.setBlockMethod(blockApp.getBlockType());
                                        break;
                                    }
                                }
                            }

//                            for (BlockAppInfo blockApp : appBlockInfoList) {
//                                for (AppInfo appInfo : appInfoList) {
//                                    if (TextUtils.equals(appInfo.getPackageName(), blockApp.getPackageName())) {
//                                        appInfo.setBlockMethod(blockApp.getBlockType());
//                                        break;
//                                    }
//                                }
//                            }

                            return appInfoList;
                        })
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnSubscribe(() -> {
//                            mRecyclerView.setVisibility(View.GONE);
//                            mProgressBar.setVisibility(View.VISIBLE);
//                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<AppInfo>>() {
                            @Override
                            public void onCompleted() {
                                mRecyclerView.setVisibility(View.VISIBLE);
                                mProgressBar.setVisibility(View.GONE);
//                                mToolbar.setVisibility(View.VISIBLE);
                                Log.d("getBlockInfo", "completed");
                            }

                            @Override
                            public void onNext(List<AppInfo> o) {
//                        mAdapter.setData(o);
                                mAdapter.setAppInfoList(o);
                                mAdapter.notifyDataSetChanged();
                                mAdapter.setOnAppItemClickListener(new AppSectionAdapter.OnAppItemClickListener() {
                                    @Override
                                    public void onItemClick(View v, String packageName) {
                                        Intent intent = new Intent(v.getContext(), DetailActivity.class);
                                        intent.putExtra(DetailActivity.EXTRA_PACKAGE_NAME, packageName);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onHelpClick(View v, @StringRes int stringId) {
                                        new AlertDialog.Builder(MainActivity.this)
                                                .setTitle("帮助")
                                                .setMessage(stringId)
                                                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                                                .show();
                                    }
                                });

                                mProgressBar.setVisibility(View.GONE);
//                                mToolbar.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (e.getMessage().equals(ADB_INACTIVE_ERROR)) {
                                    new AlertDialog.Builder(MainActivity.this)
                                            .setTitle(R.string.root_require)
                                            .setMessage(R.string.root_require_message)
                                            .setNegativeButton("尝试内置adb功能", (dialogInterface, i) ->
                                                    tryConnectAdbInside())
                                            .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> finish())
//                                    .setOnDismissListener(dialogInterface -> finish())
                                            .show();
                                } else if (e.getMessage().equals("appops not support")) {
                                    new AlertDialog.Builder(MainActivity.this)
                                            .setTitle(R.string.appops_not_support)
                                            .setMessage(R.string.appops_not_support_message)
                                            .setPositiveButton(android.R.string.ok, null)
                                            .show();
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
        mCompositeSubscription.add(subscription);
    }

    private void tryConnectAdbInside() {
//        startActivity(new Intent(this, AbdInsideTestActivity.class));
        new AlertDialog.Builder(this)
                .setTitle("Adb 设置")
                .setMessage("请将手机连接电脑，在终端运行：adb tcpip 5555\n执行完毕将手机与电脑断开连接,并点击确认")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
//                    Shell.run
//                    startService(new Intent(this, AdbService.class));
                }).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_show_system).setChecked(Settings.instance().getBoolean(Settings.SHOW_SYSTEM_APP, false));

//        if (!CrashHandler.logExists()) {
//            menu.findItem(R.id.action_send_log).setVisible(false);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh(Settings.instance().getBoolean(Settings.SHOW_SYSTEM_APP, false));
                return true;
            case R.id.action_show_system:
                item.setChecked(!item.isChecked());
                Settings.instance().putBoolean(Settings.SHOW_SYSTEM_APP, item.isChecked());
                refresh(item.isChecked());
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            /*
            case R.id.action_donate:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("alipayqr://platformapi/startapp?saId=10000007&qrcode=https%3A%2F%2Fqr.alipay.com%2Faex01083scje5axcttivf13")));
                } catch (Exception ignored) {
                    if (ClipboardUtils.put(this, "rikka@xing.moe")) {
                        Toast.makeText(this, "rikka@xing.moe copied to clipboard.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Alipay account: rikka@xing.moe", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            case R.id.action_send_log:
                if (!CrashHandler.logExists()) {
                    Toast.makeText(this, "Log file was deleted.", Toast.LENGTH_SHORT).show();
                    return true;
                }

                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "bluepadge4dev@gmail.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "App Ops carsh log");
                intent.putExtra(Intent.EXTRA_TEXT, CrashHandler.logFile(this));

                if (IntentUtils.isValid(this, intent)) {
                    intent = Intent.createChooser(intent, getString(R.string.send_via));
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getString(R.string.no_mail_app), Toast.LENGTH_SHORT).show();
                }
                */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.clear();
            mCompositeSubscription = null;
        }
        super.onDestroy();
    }
}
