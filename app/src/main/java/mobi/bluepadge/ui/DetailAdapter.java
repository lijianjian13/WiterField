package mobi.bluepadge.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mobi.bluepadge.R;
import mobi.bluepadge.database.model.AppInfo;
import mobi.bluepadge.database.model.AppOps;
import mobi.bluepadge.database.model.AppOpsWrapper;
import mobi.bluepadge.utils.PackageUtils;


public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEAD = R.layout.item_detail_header;
    public static final int TYPE_REFRESH = R.layout.item_detail_refreshing;
    public static final int TYPE_ITEM = R.layout.item_detail;
    public static final int TYPE_SUMMARY = R.layout.item_detail_summary;
    public static final int TYPE_TEXT = R.layout.item_detail_text;
    private static final int[] MODE_STRING_RES = {
            R.string.op_mode_allow,
            R.string.op_mode_ignore,
            R.string.op_mode_deny,
            R.string.op_mode_default,
    };
    private OnDetailItemClickListener mOnItemClickListener;
    private AppInfo mAppInfo;
    private AppOpsWrapper.OpInfo mOpInfo;
    private List<Data> mData;

    public DetailAdapter(AppInfo appInfo) {
        this(appInfo, null);
    }

    public DetailAdapter(AppInfo appInfo, AppOpsWrapper.OpInfo opInfo) {
        mAppInfo = appInfo;
        mOpInfo = opInfo;
        mData = new ArrayList<>();
        resetData(null);

        setHasStableIds(true);
    }

    public void setOnItemClickListener(OnDetailItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).id;
    }

    public void setOpInfo(AppOpsWrapper.OpInfo opInfo) {
        mOpInfo = opInfo;
        resetData(null);
    }

    public AppOpsWrapper.OpEntry getOpEntry(int position) {
        return (AppOpsWrapper.OpEntry) mData.get(position).obj;
    }

    public void resetData(Context context) {
        mData.clear();
        mData.add(new Data(TYPE_HEAD, TYPE_HEAD, null));

        if (context != null && mOpInfo != null && !PackageUtils.isPackageInstalled(context, mOpInfo.getPackageName())) {
            mData.add(new Data(TYPE_TEXT, TYPE_TEXT, 0));
        } else if (mOpInfo == null) {
            mData.add(new Data(TYPE_REFRESH, TYPE_REFRESH, null));
        } else {
            String group = null;
            for (AppOpsWrapper.OpEntry e : mOpInfo) {
                if (group == null || !e.getGroupLabel().equals(group)) {
                    group = e.getGroupLabel();
                    mData.add(new Data(TYPE_SUMMARY, group.hashCode(), group));
                }
                mData.add(new Data(TYPE_ITEM, 100 * e.getId() + 10000 + mData.size(), e));
            }
        }

        if (mData.size() == 1) {
            mData.add(new Data(TYPE_TEXT, TYPE_TEXT, 1));
        }

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case R.layout.item_detail_header:
                return new DetailHeaderViewHolder(view);
            case R.layout.item_detail:
                return new DetailViewHolder(view);
            case R.layout.item_detail_summary:
                return new DetailSummaryViewHolder(view);
            case R.layout.item_detail_refreshing:
                return new ViewHolder(view);
            case R.layout.item_detail_text:
                return new DetailTextViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case R.layout.item_detail_header:
                onBindViewHolder((DetailHeaderViewHolder) holder, position);
                break;
            case R.layout.item_detail:
                onBindViewHolder((DetailViewHolder) holder, position);
                break;
            case R.layout.item_detail_summary:
                onBindViewHolder((DetailSummaryViewHolder) holder, position);
                break;
            case R.layout.item_detail_text:
                onBindViewHolder((DetailTextViewHolder) holder, position);
                break;
        }
    }

    private void onBindViewHolder(DetailHeaderViewHolder holder, int position) {
        holder.icon.setImageDrawable(mAppInfo.getIcon());
        holder.name.setText(mAppInfo.getName());
        holder.versionName.setText(String.format(holder.itemView.getContext().getString(R.string.app_detail_version), mAppInfo.getVersionName()));
        holder.packageName.setText(mAppInfo.getPackageName());
        holder.block.setImageDrawable(holder.itemView.getContext().getDrawable(R.drawable.ic_open_in_new_green_24dp));

        holder.icon.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(mAppInfo.getPackageName());
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.block.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(mAppInfo.getPackageName());
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
//            if (mOnItemClickListener != null) {
//                mOnItemClickListener.onRunClick(view);
//            }
        });

//        holder.block.setOnClickListener(view -> {
//            Toast.makeText(view.getContext(), "弹出对话框", Toast.LENGTH_SHORT).show();
//        });
    }

    private void onBindViewHolder(final DetailViewHolder holder, int position) {
        AppOpsWrapper.OpEntry opEntry = (AppOpsWrapper.OpEntry) mData.get(position).obj;
        holder.status.setText(holder.itemView.getContext().getString(MODE_STRING_RES[opEntry.getMode()]));
        holder.itemView.setOnClickListener(view -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, holder.getAdapterPosition());
            }
        });

        if (opEntry.getLabel() != null) {
            holder.name.setText(opEntry.getLabel());
        } else {
            holder.name.setText(AppOps.sOpNames[opEntry.getId()]);
        }

        holder.icon.setImageDrawable(opEntry.getIcon());

        if ((position == getItemCount() - 1) || mData.get(position + 1).type != TYPE_ITEM) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
    }

    private void onBindViewHolder(final DetailSummaryViewHolder holder, int position) {
        if (!TextUtils.isEmpty((String) mData.get(position).obj)) {
            holder.title.setText((String) mData.get(position).obj);
        } else {
            holder.title.setText(holder.itemView.getContext().getString(R.string.permission_other));
        }
    }

    private void onBindViewHolder(final DetailTextViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        switch ((int) mData.get(position).obj) {
            case 0:
                holder.text.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getDrawable(R.drawable.ic_delete_32dp), null, null);
                holder.text.setText(context.getString(R.string.app_uninstalled));
                holder.itemView.setOnClickListener(null);
                break;
            case 1:
                holder.text.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getDrawable(R.drawable.ic_help_32dp), null, null);
                holder.text.setText(context.getString(R.string.no_operations));
                holder.itemView.setOnClickListener(view -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, holder.getAdapterPosition());
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).type;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static interface OnDetailItemClickListener {
        void onItemClick(View v, int position);

    }

    public static class Data {
        int type;
        long id;
        Object obj;

        public Data(int type, long id, Object obj) {
            this.type = type;
            this.id = id;
            this.obj = obj;
        }
    }
}
