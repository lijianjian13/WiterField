package mobi.bluepadge.ui.intro;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mobi.bluepadge.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SecondIntroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SecondIntroFragment extends IntroFragment {


    //    private OnFragmentInteractionListener mListener;
    private CheckBox checkBox;
    private EditText portEt;
    private TextView mIntro3Tv, mIntro4Tv;

    public SecondIntroFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SecondIntroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SecondIntroFragment newInstance() {
        SecondIntroFragment fragment = new SecondIntroFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second_intro, container, false);
        portEt = (EditText) view.findViewById(R.id.port_et);
        mIntro3Tv = (TextView) view.findViewById(R.id.intro_3_tv);
        mIntro4Tv = (TextView) view.findViewById(R.id.intro_4_tv);

        portEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    mIntro3Tv.setVisibility(View.INVISIBLE);
                    mIntro4Tv.setVisibility(View.INVISIBLE);
                    checkBox.setVisibility(View.INVISIBLE);
                } else {

                    int port = Integer.parseInt(s.toString());
                    if (port > 1024 && port < 65535) {
                        mIntro3Tv.setVisibility(View.VISIBLE);
                        mIntro4Tv.setVisibility(View.VISIBLE);
                        checkBox.setVisibility(View.VISIBLE);
                        mIntro3Tv.setText(getResources().getString(R.string.intro_3_input_port, port));
                        mIntro4Tv.setText(getResources().getString(R.string.intro_4_reboot, port));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (buttonView.isPressed() && isChecked) {
                checkBox.setChecked(false);
                String content = portEt.getText().toString();
                if (TextUtils.isEmpty(content)) {
                    Toast.makeText(getContext(), "请输入端口号", Toast.LENGTH_SHORT).show();
                    return;
                }
                int port = Integer.parseInt(content);
                if (port <= 1024 || port >= 65535) {
                    Toast.makeText(getContext(), "端口号不符规则", Toast.LENGTH_SHORT).show();
//                    checkBox.setChecked(false);
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("警告")
                            .setMessage("开启adb远程调试，与Root类似您需要承担一定的安全风险，本软件不承担该责任，点击确认代表同意。\n如不同意，请卸载本软件后，并关闭开发者选项。")
                            .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                                checkBox.setChecked(false);
                            })
                            .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                                checkBox.setChecked(true);
                            }).show();
                }
            }
        });
        return view;
    }

    @Override
    public boolean canGoNext() {
        return checkBox.isChecked();
    }

    @Override
    public int getPort() {
        return Integer.parseInt(portEt.getText().toString());
    }
}
