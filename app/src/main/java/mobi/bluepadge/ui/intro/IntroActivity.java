package mobi.bluepadge.ui.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobi.bluepadge.R;
import mobi.bluepadge.ui.Settings;
import mobi.bluepadge.ui.SplashActivity;

public class IntroActivity extends AppCompatActivity {

    private ViewPager mContainer;
    private Button mPrevious, mNext;
    private FragmentManager mFragmentManager;
    private List<IntroFragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mContainer = (ViewPager) findViewById(R.id.container);
        mPrevious = (Button) findViewById(R.id.previous);
        mNext = (Button) findViewById(R.id.next);
        mFragmentManager = getSupportFragmentManager();

        fragments.add(FirstIntroFragment.newInstance());
        fragments.add(SecondIntroFragment.newInstance());

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(mFragmentManager, fragments);
        mContainer.setAdapter(myPagerAdapter);
        mPrevious.setOnClickListener(v -> {
            mContainer.setCurrentItem(mContainer.getCurrentItem() - 1);
        });
        mNext.setOnClickListener(v -> {
            mContainer.setCurrentItem(mContainer.getCurrentItem() + 1);
        });
        mContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mPrevious.setVisibility(View.GONE);
                        mNext.setVisibility(View.VISIBLE);
                        mNext.setText("下一步");
                        mNext.setOnClickListener(v -> {
                            mContainer.setCurrentItem(1);
                        });
                        break;
                    case 1:
                        mPrevious.setVisibility(View.VISIBLE);
                        mNext.setVisibility(View.VISIBLE);
                        mNext.setText("完成");
                        mNext.setOnClickListener(v -> {
                            IntroFragment introFragment = (IntroFragment) myPagerAdapter.getItem(mContainer.getCurrentItem());
                            if (introFragment.canGoNext()) {
                                int port = introFragment.getPort();
                                Settings.instance().putBoolean(Settings.SHOW_INTRO, true);
                                Settings.instance().putInt(Settings.ADB_PORT, port);
                                startActivity(new Intent(IntroActivity.this, SplashActivity.class));
                                finish();
                            } else {
                                Toast.makeText(IntroActivity.this, "请完成相关操作", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
//        private static int NUM_ITEMS = 2;

        List<IntroFragment> fragments = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fragmentManager, List<IntroFragment> introFragments) {
            super(fragmentManager);
            this.fragments.addAll(introFragments);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return fragments.size();
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }
}
