package mobi.bluepadge.ui.intro;

import android.support.v4.app.Fragment;

/**
 * Created by lijianjian on 16/12/28.
 */

public abstract class IntroFragment extends Fragment {
    public abstract boolean canGoNext();

    public abstract int getPort();
}
