package mobi.bluepadge.ui;

import android.annotation.SuppressLint;
import android.support.annotation.StringRes;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.List;

import mobi.bluepadge.R;
import mobi.bluepadge.database.model.AmOps;
import mobi.bluepadge.database.model.AppInfo;

/**
 * Created by bluepadge on 16/12/11.
 */

public class AppSectionAdapter extends SectioningAdapter {

    private SparseArray<List<AppInfo>> mSections = new SparseArray<>();
    private OnAppItemClickListener mOnItemClickListener;

    public void setOnAppItemClickListener(OnAppItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void setAppInfoList(List<AppInfo> appInfoList) {
//        this.mAppInfoList.clear();
//        this.mAppInfoList.addAll(appInfoList);
        mSections.clear();

        for (AppInfo appInfo : appInfoList) {
            int blockMethod = appInfo.getBlockMethod();
//            if (mSections.get(blockMethod).size() == 0) {
            if (mSections.indexOfKey(blockMethod) < 0) {
                List<AppInfo> sectionApps = new ArrayList<>();
                sectionApps.add(appInfo);
                mSections.put(blockMethod, sectionApps);
            } else {
                mSections.get(blockMethod).add(appInfo);
            }
//            mSections.get(appInfo.getBlockMethod()).add(appInfo);
        }

        notifyAllSectionsDataSetChanged();
    }

    @Override
    public int getNumberOfSections() {
        return mSections.size();
    }


    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
//        return sections.get(sectionIndex).people.size();
        return mSections.valueAt(sectionIndex).size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_app, parent, false);
        return new AppContentViewHolder(v);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_app_section, parent, false);
        return new AppHeaderViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, int itemIndex, int itemType) {
//        Section s = sections.get(sectionIndex);
//        Person person = s.people.get(itemIndex);
//        ivh.personNameTextView.setText(capitalize(person.name.last) + ", " + capitalize(person.name.first));
        AppInfo appInfo = mSections.valueAt(sectionIndex).get(itemIndex);
        AppContentViewHolder holder = (AppContentViewHolder) viewHolder;

        holder.icon.setImageDrawable(appInfo.getIcon());
        holder.name.setText(appInfo.getName());
        /*
        if (appInfo.getRunning()) {
            Log.d(appInfo.getPackageName(), "flags:" + appInfo.getRunningInfo().flags);
            holder.runningHintTv.setVisibility(View.VISIBLE);
            String status = "运行中";
            if (appInfo.getRunningInfo().foreground) {
                status = "前台运行中";
            } else {
                switch (appInfo.getRunningInfo().flags) {
                    case 0:
                        status = "缓存进程";
                        break;
                    default:
                        status = "后台运行中";
                        break;
                }
            }
            holder.runningHintTv.setText(status);
        } else {
            Log.d("running", "隐藏");
            holder.runningHintTv.setVisibility(View.INVISIBLE);
        }
        */
//        holder.itemView.setOnClickListener(view -> {
//            if (mOnItemClickListener != null) {
//                mOnItemClickListener.onItemClick(view, appInfo.getPackageName());
//            }
//        });
        holder.itemView.setOnClickListener(view -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, appInfo.getPackageName());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        int blockMethod = mSections.keyAt(sectionIndex);
        AppHeaderViewHolder headerViewHolder = (AppHeaderViewHolder) viewHolder;

        switch (blockMethod) {
            case AmOps.BLOCK_TYPE_STOP:
                headerViewHolder.blockTypeTv.setText(R.string.black_list);
                headerViewHolder.helpHintIv.setVisibility(View.VISIBLE);
                headerViewHolder.helpHintIv.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onHelpClick(v, R.string.app_black_desc);
                    }
                });
                break;
            case AmOps.BLOCK_TYPE_INACTIVE:
                headerViewHolder.blockTypeTv.setText(R.string.grey_list);
                headerViewHolder.helpHintIv.setVisibility(View.VISIBLE);
                headerViewHolder.helpHintIv.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onHelpClick(v, R.string.app_grey_desc);
                    }
                });
                break;
            case AmOps.BLOCK_TYPE_NORMAL:
                headerViewHolder.blockTypeTv.setText(R.string.normal_list);
                headerViewHolder.helpHintIv.setVisibility(View.INVISIBLE);
                break;
        }
    }


    public static interface OnAppItemClickListener {
        void onItemClick(View v, String packageName);

        void onHelpClick(View v, @StringRes int StringId);
    }

    private class AppContentViewHolder extends SectioningAdapter.ItemViewHolder {

        public ImageView icon;
        public TextView name;
//        public TextView runningHintTv;

        public AppContentViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(android.R.id.icon);
            name = (TextView) itemView.findViewById(android.R.id.title);
//            runningHintTv = (TextView) itemView.findViewById(R.id.running_hint_tv);
        }
    }

    private class AppHeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView blockTypeTv;
        ImageView helpHintIv;

        public AppHeaderViewHolder(View itemView) {
            super(itemView);
            blockTypeTv = (TextView) itemView.findViewById(R.id.block_type_tv);
            helpHintIv = (ImageView) itemView.findViewById(R.id.help_hint_iv);
        }
    }


}
