package mobi.bluepadge.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import mobi.bluepadge.R;
import mobi.bluepadge.database.AppDataSource;
import mobi.bluepadge.database.model.AmOps;
import mobi.bluepadge.database.model.AppInfo;
import mobi.bluepadge.database.model.AppOps;
import mobi.bluepadge.database.model.AppOpsWrapper;
import mobi.bluepadge.database.model.BlockAppInfo;
import mobi.bluepadge.remoteadblibrary.Shell;
import mobi.bluepadge.ui.Bus.AppEvent;
import mobi.bluepadge.ui.Bus.RxBus;
import mobi.bluepadge.utils.BottomSheetDialog;
import mobi.bluepadge.utils.PackageUtils;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_PACKAGE_NAME = "EXTRA_PACKAGE_NAME";
    private static final String TAG = "DetailActivity";
    private static final int[] OP_MODE_DESC_STRING_RES = {
            R.string.op_mode_allow_desc,
            R.string.op_mode_ignore_desc,
            R.string.op_mode_deny_desc,
            R.string.op_mode_default_desc,
    };
    private static final int[] RB_IDS = {
            R.id.radio1,
            R.id.radio2,
            R.id.radio3,
            R.id.radio4,
    };
    private RecyclerView mRecyclerView;
    private DetailAdapter mAdapter;
    private String mPackageName;
    private boolean mAppUninstalled;
    private CompositeSubscription mCompositeSubscription;
    private BlockAppInfo mBlockAppInfo;
    private AppDataSource mAppDataSource;
    private FloatingActionButton mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mCompositeSubscription = new CompositeSubscription();

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(v -> {
            showBlockDialog();
        });

        AppInfo appInfo = getAppInfo();
        mPackageName = getIntent().getStringExtra(EXTRA_PACKAGE_NAME);
        mAppUninstalled = PackageUtils.isPackageInstalled(this, mPackageName);

        mAppDataSource = new AppDataSource(this);
        Subscription subscription = mAppDataSource.getBlockApp(mPackageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BlockAppInfo>() {
                    @Override
                    public void onCompleted() {
                        Log.d("getBlockInfo", "detail completed");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BlockAppInfo blockAppInfo) {
                        mBlockAppInfo = blockAppInfo;
                    }
                });
//                .subscribe(blockAppInfo -> {
//                    this.mBlockAppInfo = blockAppInfo;
//                });
        mCompositeSubscription.add(subscription);

        if (appInfo == null) {
            finish();
            return;
        }

        RecyclerView rv = (RecyclerView) findViewById(android.R.id.list);
        rv.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new DetailAdapter(appInfo);
        rv.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((v, position) -> {
            switch (mAdapter.getItemViewType(position)) {
                case DetailAdapter.TYPE_ITEM:
                    showEditDialog(position);
                    break;
                case DetailAdapter.TYPE_TEXT:
                    refresh();
                    break;
            }
        });


        refresh();
    }

    private void showBlockDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(R.layout.block_sheet_edit);
        dialog.show();

        View content = dialog.getContentView();

        ImageView helpHintIv = (ImageView) content.findViewById(R.id.help_hint_iv);
        helpHintIv.setOnClickListener(v -> {
            String blockDesc = getResources().getString(R.string.app_black_desc) + "\n" +
                    getResources().getString(R.string.app_grey_desc);
            new AlertDialog.Builder(this)
                    .setTitle("帮助")
                    .setMessage(blockDesc)
                    .setPositiveButton(android.R.string.ok, (dialogIf, i) -> dialogIf.dismiss())
                    .show();
        });

        final RadioGroup radioGroup = (RadioGroup) content.findViewById(android.R.id.content);
        final RadioButton blackRb = (RadioButton) content.findViewById(R.id.radio1);
        final RadioButton greyRb = (RadioButton) content.findViewById(R.id.radio2);
        final RadioButton normalRb = (RadioButton) content.findViewById(R.id.radio3);

        int blockType =
                mBlockAppInfo == null ? AmOps.BLOCK_TYPE_NORMAL :
                        mBlockAppInfo.getBlockType();
        switch (blockType) {
            case AmOps.BLOCK_TYPE_STOP:
                blackRb.setChecked(true);
                break;
            case AmOps.BLOCK_TYPE_INACTIVE:
                greyRb.setChecked(true);
                break;
            case AmOps.BLOCK_TYPE_NORMAL:
                normalRb.setChecked(true);
                break;
        }

        radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
            RxBus.getInstance().post(new AppEvent(mPackageName));
            BlockAppInfo blockAppInfo = new BlockAppInfo();
            int id = radioGroup1.getCheckedRadioButtonId();
            switch (id) {
                case R.id.radio1:
                    blockAppInfo.setPackageName(mPackageName);
                    blockAppInfo.setBlockType(AmOps.BLOCK_TYPE_STOP);
                    mAppDataSource.delete(blockAppInfo);
                    mAppDataSource.save(blockAppInfo);
                    break;
                case R.id.radio2:
                    blockAppInfo.setPackageName(mPackageName);
                    blockAppInfo.setBlockType(AmOps.BLOCK_TYPE_INACTIVE);
                    mAppDataSource.delete(blockAppInfo);
                    mAppDataSource.save(blockAppInfo);
                    break;
                case R.id.radio3:
                    blockAppInfo.setPackageName(mPackageName);
                    blockAppInfo.setBlockType(AmOps.BLOCK_TYPE_NORMAL);
                    mAppDataSource.delete(blockAppInfo);
                    break;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAppUninstalled != PackageUtils.isPackageInstalled(this, mPackageName)) {
            mAppUninstalled = !mAppUninstalled;
            mAdapter.resetData(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.clear();
        }
        AppOpsWrapper.GROUP_LABEL = null;
    }

    private void refresh() {
        Subscription subscription =

                Observable.create(new Observable.OnSubscribe<AppOpsWrapper.OpInfo>() {
                    @Override
                    public void call(Subscriber<? super AppOpsWrapper.OpInfo> subscriber) {
                        AppOpsWrapper.OpInfo opInfo = AppOpsWrapper.getPackageOp(getApplicationContext(), mPackageName);
                        if (opInfo != null) {
                            subscriber.onNext(opInfo);
                        } else {
                            subscriber.onError(new RuntimeException());
                        }
                    }
                })
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(() -> {
                            mAdapter.setOpInfo(null);
                            mAdapter.notifyDataSetChanged();
                        })
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<AppOpsWrapper.OpInfo>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                Toast.makeText(DetailActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNext(AppOpsWrapper.OpInfo opInfo) {
                                mAdapter.setOpInfo(opInfo);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
        mCompositeSubscription.add(subscription);
    }

    private void reset() {
        Subscription subscription = Observable.create(new Observable.OnSubscribe<AppOpsWrapper.OpInfo>() {
            @Override
            public void call(Subscriber<? super AppOpsWrapper.OpInfo> subscriber) {
                AppOpsWrapper.OpInfo opInfo = AppOpsWrapper.resetPackageOp(DetailActivity.this, mPackageName);
                if (opInfo != null) {
                    subscriber.onNext(opInfo);
                } else {
                    subscriber.onError(new RuntimeException());
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(() -> {
                    mAdapter.setOpInfo(null);
                    mAdapter.notifyDataSetChanged();
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AppOpsWrapper.OpInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(DetailActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(AppOpsWrapper.OpInfo opInfo) {
                        mAdapter.setOpInfo(opInfo);
                        mAdapter.notifyDataSetChanged();
                    }
                });
        mCompositeSubscription.add(subscription);
    }

    private void showEditDialog(final int position) {
        final AppOpsWrapper.OpEntry opEntry = mAdapter.getOpEntry(position);
        final int current = opEntry.getMode();

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(R.layout.content_sheet_edit);
        dialog.show();

        View content = dialog.getContentView();

        ((TextView) content.findViewById(android.R.id.title)).setText(opEntry.getLabel());
        if (!TextUtils.isEmpty(opEntry.getDescription())) {
            content.findViewById(android.R.id.icon).setOnClickListener(
                    view -> new AlertDialog.Builder(view.getContext())
                            .setMessage(opEntry.getDescription())
                            .setPositiveButton(android.R.string.ok, null)
                            .show());
        } else {
            content.findViewById(android.R.id.icon).setVisibility(View.GONE);
        }


        final TextView desc = (TextView) content.findViewById(android.R.id.text2);

        final RadioGroup radioGroup = (RadioGroup) content.findViewById(android.R.id.content);

        switch (opEntry.getId()) {
            case AppOpsWrapper.OP_STORAGE:
                radioGroup.findViewById(R.id.radio2).setVisibility(View.GONE);
                break;
            case AppOps.OP_RUN_IN_BACKGROUND:
                radioGroup.findViewById(R.id.radio3).setVisibility(View.GONE);
                radioGroup.findViewById(R.id.radio4).setVisibility(View.GONE);
                break;
        }

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            int id = radioGroup.getCheckedRadioButtonId();
            if (id == -1) {
                return;
            }

            View v = radioGroup.findViewById(id);
            int mode = Integer.parseInt((String) v.getTag());
            desc.setText(getString(OP_MODE_DESC_STRING_RES[mode]));
        });
        ((RadioButton) radioGroup.findViewById(RB_IDS[current])).setChecked(true);

        dialog.setOnDismissListener(dialogInterface -> {
            int id = radioGroup.getCheckedRadioButtonId();
            if (id == -1) {
                return;
            }

            View v = radioGroup.findViewById(id);
            int mode = Integer.parseInt((String) v.getTag());
            if (mode != current) {
                setOp(mPackageName, opEntry.getId(), mode, position);
            }
        });
    }

    private void setOp(final String packageName, final int opId, final int opMode, final int position) {
        Subscription subscription =
                Observable.create(new Observable.OnSubscribe<AppOpsWrapper.OpInfo>() {
                    @Override
                    public void call(Subscriber<? super AppOpsWrapper.OpInfo> subscriber) {
                        try {
                            subscriber.onNext(AppOpsWrapper.setPackageOp(getApplicationContext(), packageName, opId, opMode));
                        } catch (RuntimeException e) {
                            subscriber.onError(e);
                        }
                    }
                })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<AppOpsWrapper.OpInfo>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(DetailActivity.this, "Failed.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNext(AppOpsWrapper.OpInfo opInfo) {
                                mAdapter.setOpInfo(opInfo);
                                mAdapter.notifyItemChanged(position);
                            }
                        });
        mCompositeSubscription.add(subscription);
    }

    private AppInfo getAppInfo() {
        if (!getIntent().hasExtra(EXTRA_PACKAGE_NAME)) {
            return null;
        }

        String packageName = getIntent().getStringExtra(EXTRA_PACKAGE_NAME);
        Log.d(TAG, packageName);

        AppInfo ai = null;
        try {
            ai = new AppInfo(packageName, getPackageManager());
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return ai;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
            case R.id.action_reset:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.reset_title)
                        .setMessage(R.string.reset_message)
                        .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> reset())
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
                return true;
//            case R.id.action_raw:
//                StringBuilder sb = new StringBuilder();
//                new Thread(() -> {
//                    for (String s : Shell.run(DetailActivity.this, "appops get " + mPackageName)) {
//                        sb.append(s).append("\n\n");
//                    }
//                    runOnUiThread(() -> new AlertDialog.Builder(DetailActivity.this)
//                            .setMessage(sb.toString().trim())
//                            .show());
//                }).start();
//
//                return true;
            case R.id.action_detail:
                startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", mPackageName, null)));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
