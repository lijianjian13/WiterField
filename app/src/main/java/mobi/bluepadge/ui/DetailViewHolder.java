package mobi.bluepadge.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mobi.bluepadge.R;


public class DetailViewHolder extends RecyclerView.ViewHolder {

    public TextView name;
    public TextView status;
    public ImageView icon;
    public View divider;

    public DetailViewHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(android.R.id.text1);
        status = (TextView) itemView.findViewById(android.R.id.text2);
        icon = (ImageView) itemView.findViewById(android.R.id.icon);
        divider = itemView.findViewById(R.id.divider);
    }
}
