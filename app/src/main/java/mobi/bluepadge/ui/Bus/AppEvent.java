package mobi.bluepadge.ui.Bus;

/**
 * Created by lijianjian on 16/12/25.
 */

public class AppEvent {
    private String packageName;

    public AppEvent(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
