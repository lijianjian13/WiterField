package mobi.bluepadge.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


public class DetailTextViewHolder extends RecyclerView.ViewHolder {

    public TextView text;

    public DetailTextViewHolder(View itemView) {
        super(itemView);

        text = (TextView) itemView.findViewById(android.R.id.text1);
    }
}
