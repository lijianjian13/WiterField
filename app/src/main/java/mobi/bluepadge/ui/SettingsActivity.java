package mobi.bluepadge.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import mobi.bluepadge.R;
import mobi.bluepadge.services.IdleService;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private LinearLayout mPortLl, mTimerLl;
    private TextView mPortNumTv, mTimerTv;
    private Settings mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mSettings = Settings.instance();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mPortLl = (LinearLayout) findViewById(R.id.port_ll);
        mPortNumTv = (TextView) findViewById(R.id.port_tv);
        mPortNumTv.setText(String.valueOf(mSettings.getInt(Settings.ADB_PORT, 5555)));
        mTimerLl = (LinearLayout) findViewById(R.id.timer_ll);
        mTimerTv = (TextView) findViewById(R.id.timer_tv);
        mTimerTv.setText(String.valueOf(mSettings.getInt(Settings.TIMER, 6)) + "分钟");

        mPortLl.setOnClickListener(v -> showPortEditor());

        mTimerLl.setOnClickListener(v -> showTimerEditor());

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("设置");

    }

    private void showPortEditor() {
        View timerView = LayoutInflater.from(this).inflate(R.layout.port_editor, null, false);
        EditText portEt = (EditText) timerView.findViewById(R.id.port_et);
        new AlertDialog.Builder(this)
                .setTitle("端口号")
                .setView(timerView)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    String content = portEt.getText().toString();
                    if (TextUtils.isEmpty(content)) {
                        Toast.makeText(this, "请输入端口号", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int portNum = Integer.parseInt(content);
                    if (portNum < 1024 || portNum > 65535) {
                        Toast.makeText(this, "端口号不符合要求", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mSettings.putInt(Settings.ADB_PORT, portNum);
                    Toast.makeText(this, "端口号已更新", Toast.LENGTH_SHORT).show();
                    mPortNumTv.setText(String.valueOf(mSettings.getInt(Settings.ADB_PORT, 5555)));
                    dialog.dismiss();
                }).show();
    }

    private void showTimerEditor() {
        View timerView = LayoutInflater.from(this).inflate(R.layout.timer_editor, null, false);
        int min = mSettings.getInt(Settings.TIMER, 6);
        RadioGroup timerRg = (RadioGroup) timerView.findViewById(R.id.timer_rg);
        RadioButton zeroRg = (RadioButton) timerView.findViewById(R.id.zero_min);
        RadioButton twoMRg = (RadioButton) timerView.findViewById(R.id.two_min);
        RadioButton sixMRg = (RadioButton) timerView.findViewById(R.id.six_min);
        RadioButton quaHRg = (RadioButton) timerView.findViewById(R.id.quart_hour);
        switch (min) {
            case 0:
                zeroRg.setChecked(true);
                break;
            case 2:
                twoMRg.setChecked(true);
                break;
            case 6:
                sixMRg.setChecked(true);
                break;
            case 15:
                quaHRg.setChecked(true);
                break;
        }
        new AlertDialog.Builder(this)
                .setView(timerView)
                .setTitle("休眠设置")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    switch (timerRg.getCheckedRadioButtonId()) {
                        case R.id.zero_min:
                            mSettings.putInt(Settings.TIMER, 0);
                            break;
                        case R.id.two_min:
                            mSettings.putInt(Settings.TIMER, 2);
                            break;
                        case R.id.six_min:
                            mSettings.putInt(Settings.TIMER, 6);
                            break;
                        case R.id.quart_hour:
                            mSettings.putInt(Settings.TIMER, 15);
                            break;
                    }
                    mTimerTv.setText(String.valueOf(mSettings.getInt(Settings.TIMER, 6)) + "分钟");
                    Intent intent = new Intent(IdleService.BROADCAST_UNIQUE_KEY);
                    intent.putExtra(IdleService.DELAY_KEY, mSettings.getInt(Settings.TIMER, 6));
                    sendBroadcast(intent);
                    Toast.makeText(this, "自动休眠间隔已更新", Toast.LENGTH_SHORT).show();
                }).show();
    }
}
