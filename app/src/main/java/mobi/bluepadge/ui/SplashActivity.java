package mobi.bluepadge.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import mobi.bluepadge.ui.intro.IntroActivity;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean hasShownIntro = Settings.instance().getBoolean(Settings.SHOW_INTRO, false);
        if (hasShownIntro) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
