package mobi.bluepadge.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


public class DetailSummaryViewHolder extends RecyclerView.ViewHolder {

    public TextView title;

    public DetailSummaryViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(android.R.id.title);
    }
}
