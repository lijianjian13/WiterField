package mobi.bluepadge.ui;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class CrashHandler implements Thread.UncaughtExceptionHandler {

    public static String CRASH_DIR;
    public static String CRASH_LOG;

    private static String ANDROID = Build.VERSION.RELEASE;
    private static String MODEL = Build.MODEL;
    private static String MANUFACTURER = Build.MANUFACTURER;

    public static String VERSION_NAME = "Unknown";
    public static String VERSION_CODE = "Unknown";
    public static String INSTALLER_PACKAGE_NAME;

    private Thread.UncaughtExceptionHandler mPrevious;

    public static void init(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            VERSION_NAME = info.versionName;
            VERSION_NAME = Integer.toString(info.versionCode);
            INSTALLER_PACKAGE_NAME = context.getPackageManager().getInstallerPackageName(context.getPackageName());

            CRASH_DIR = context.getCacheDir().getAbsolutePath() + "/";
            CRASH_LOG = CRASH_DIR + "last_crash.log";
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void register() {
        new CrashHandler();
    }

    private CrashHandler() {
        //mPrevious = Thread.currentThread().getUncaughtExceptionHandler();
        mPrevious = Thread.getDefaultUncaughtExceptionHandler();
        //Thread.currentThread().setUncaughtExceptionHandler(this);
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public static boolean logExists() {
        return new File(CRASH_LOG).exists();
    }

    public static String logFile(Context context) {
        try {
            StringBuilder sb = new StringBuilder();

            BufferedReader reader = new BufferedReader(new FileReader(new File(context.getCacheDir(), "last_crash.log")));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString().trim();
        } catch (IOException ignored) {
            return "";
        }
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        if (CRASH_LOG == null) {
            return;
        }

        File f = new File(CRASH_LOG);
        if (f.exists()) {
            //noinspection ResultOfMethodCallIgnored
            f.delete();
        } else {
            try {
                //noinspection ResultOfMethodCallIgnored
                f.mkdirs();
                //noinspection ResultOfMethodCallIgnored
                f.createNewFile();
            } catch (Exception e) {
                return;
            }
        }

        PrintWriter p;
        try {
            p = new PrintWriter(f);
        } catch (Exception e) {
            return;
        }

        p.write("Android Version: " + ANDROID + "\n");
        p.write("Device Model: " + MODEL + "\n");
        p.write("Device Manufacturer: " + MANUFACTURER + "\n");
        p.write("App Version: " + VERSION_NAME + " (" + VERSION_CODE + ")\n");
        p.write("InstallerPackageName: " + INSTALLER_PACKAGE_NAME + "\n");
        p.write("*********************\n");
        throwable.printStackTrace(p);
        p.close();

        if (mPrevious != null) {
            mPrevious.uncaughtException(thread, throwable);
        }
    }
}