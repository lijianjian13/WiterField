package mobi.bluepadge.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mobi.bluepadge.R;


public class DetailHeaderViewHolder extends RecyclerView.ViewHolder {

    public ImageView icon,block;
    public TextView name;
    public TextView versionName;
    public TextView packageName;

    public DetailHeaderViewHolder(View itemView) {
        super(itemView);

        icon = (ImageView) itemView.findViewById(android.R.id.icon);
        name = (TextView) itemView.findViewById(android.R.id.title);
        versionName = (TextView) itemView.findViewById(android.R.id.text1);
        packageName = (TextView) itemView.findViewById(android.R.id.text2);
        block = (ImageView) itemView.findViewById(R.id.block_iv);
    }
}
