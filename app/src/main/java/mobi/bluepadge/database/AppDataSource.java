package mobi.bluepadge.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.List;

import mobi.bluepadge.database.model.BlockAppInfo;
import mobi.bluepadge.database.tables.BlockAppTable;
import rx.Observable;

/**
 * Created by bluepadge on 16/12/10.
 */

public class AppDataSource extends AbstractLocalDataSource<BlockAppTable> {

    public AppDataSource(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected BlockAppTable instantiateTable() {
        return new BlockAppTable();
    }

    public Observable<List<BlockAppInfo>> getBlockApps() {

        return mDatabaseHelper.createQuery(BlockAppTable.TABLE_NAME, BlockAppTable.QUERY_APP)
                .mapToList(cursor -> mTable.parseCursor(cursor));
//                .mapToOne(cursor -> {
//                    if (cursor.getCount() > 0) {
//                        return mTable.parseCursor(cursor);
//                    }
//                    return null;
//                });
    }

    public boolean save(BlockAppInfo blockAppInfo) {
        return mDatabaseHelper.insert(BlockAppTable.TABLE_NAME, mTable.toContentValues(blockAppInfo),
                SQLiteDatabase.CONFLICT_REPLACE) == 1;
    }

    public boolean delete(BlockAppInfo blockAppInfo) {
        return mDatabaseHelper.delete(BlockAppTable.TABLE_NAME, BlockAppTable.WHERE_PACKAGE_NAME_EQUALS,
                blockAppInfo.getPackageName()) == 1;
    }

    public boolean deleteAll() {
        return mDatabaseHelper.delete(BlockAppTable.TABLE_NAME, null) == 1;
    }

    public Observable<BlockAppInfo> getBlockApp(String packageName) {
//        String nameInSql = String.format(Locale.ENGLISH, "'%s'", packageName);
//        mDatabaseHelper.createQuery
//        return mDatabaseHelper.createQuery(BlockAppTable.QUERY_BLOCK_TYPE, packageName)
        return mDatabaseHelper.createQuery(BlockAppTable.TABLE_NAME, BlockAppTable.QUERY_BLOCK_TYPE, new String[]{packageName})
                .mapToOne(cursor -> mTable.parseCursor(cursor));
    }

}
