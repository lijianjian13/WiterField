package mobi.bluepadge.database;

import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import mobi.bluepadge.database.tables.BaseTable;
import mobi.bluepadge.database.tables.DatabaseHelper;
import rx.schedulers.Schedulers;

/**
 * Created by bluepadge on 16/12/10.
 */

public abstract class AbstractLocalDataSource<T extends BaseTable> {
    protected T mTable;
    protected BriteDatabase mDatabaseHelper;

    public AbstractLocalDataSource(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SqlBrite sqlBrite = SqlBrite.create();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(databaseHelper, Schedulers.io());
        mTable = instantiateTable();
    }

    @NonNull
    protected abstract T instantiateTable();
}
