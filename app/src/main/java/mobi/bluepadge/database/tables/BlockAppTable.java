package mobi.bluepadge.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import mobi.bluepadge.database.model.BlockAppInfo;

/**
 * Created by bluepadge on 16/12/10.
 */

public class BlockAppTable implements BaseTable<BlockAppInfo>, BaseColumns {
    public static final String TABLE_NAME = "app";
    //    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PACKAGE_NAME = "package_name";
    public static final String COLUMN_BLOCK_TYPE = "block_method";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " ( " +
//                    COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_PACKAGE_NAME + " TEXT PRIMARY KEY, " +
                    COLUMN_BLOCK_TYPE + " INTEGER " +
                    " );";

    public static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public static final String QUERY_APP =
            "SELECT * FROM " + TABLE_NAME + ";";

    public static final String WHERE_PACKAGE_NAME_EQUALS = COLUMN_PACKAGE_NAME + "=?;";
    public static final String QUERY_BLOCK_TYPE = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_PACKAGE_NAME + " =?;";

    @Override
    public String createTableSql() {
        return CREATE_TABLE;
    }

    @Override
    public String deleteTableSql() {
        return DELETE_TABLE;
    }

    @Override
    public ContentValues toContentValues(BlockAppInfo blockAppInfo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PACKAGE_NAME, blockAppInfo.getPackageName());
        contentValues.put(COLUMN_BLOCK_TYPE, blockAppInfo.getBlockType());
        return contentValues;
    }

    @Override
    public BlockAppInfo parseCursor(Cursor cursor) {
        BlockAppInfo blockAppInfo = new BlockAppInfo();
        blockAppInfo.setPackageName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PACKAGE_NAME)));
        blockAppInfo.setBlockType(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_BLOCK_TYPE)));
        return blockAppInfo;
    }
}
