package mobi.bluepadge.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mobi.bluepadge.database.model.AmOps;
import mobi.bluepadge.database.model.BlockAppInfo;

/**
 * Created by bluepadge on 16/12/10.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "appBlocked.db";
    private static final String TAG = "DatabaseHelper";
    private List<Class<? extends BaseTable>> mRegisteredTables;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        registerTables();
    }

    private void registerTables() {
        mRegisteredTables = new ArrayList<>();
        mRegisteredTables.add(BlockAppTable.class);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(TAG, db.getVersion() + "");
            createTables(db);
        } catch (Exception e) {
            //            e.printStackTrace();
            Log.e(TAG, "onCreate: ", e);
        }

    }

    private void createTables(SQLiteDatabase db) throws IllegalAccessException, InstantiationException {
        if (mRegisteredTables == null) return;
        for (Class<? extends BaseTable> table : mRegisteredTables) {
            BaseTable tableInstance = table.newInstance();
            db.execSQL(tableInstance.createTableSql());
//            addTestData(db);
        }
    }

    private ContentValues parse(BlockAppInfo blockAppInfo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BlockAppTable.COLUMN_PACKAGE_NAME, blockAppInfo.getPackageName());
        contentValues.put(BlockAppTable.COLUMN_BLOCK_TYPE, blockAppInfo.getBlockType());
        return contentValues;
    }

    private void addTestData(SQLiteDatabase db) {
        BlockAppInfo blockAppInfo = new BlockAppInfo();
        blockAppInfo.setPackageName("me.ele");
        blockAppInfo.setBlockType(AmOps.BLOCK_TYPE_STOP);
        db.insert(BlockAppTable.TABLE_NAME, null, parse(blockAppInfo));

        blockAppInfo = new BlockAppInfo("com.eg.android.AlipayGphone", AmOps.BLOCK_TYPE_STOP);
        db.insert(BlockAppTable.TABLE_NAME, null, parse(blockAppInfo));

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        onDelete(db);
        onCreate(db);
    }

    private void deleteTables(SQLiteDatabase db) throws IllegalAccessException, InstantiationException {
        if (mRegisteredTables == null) return;
        for (Class<? extends BaseTable> table : mRegisteredTables) {
            BaseTable tableInstance = table.newInstance();
            db.execSQL(tableInstance.createTableSql());
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void onDelete(SQLiteDatabase db) {
        try {
            deleteTables(db);
        } catch (Exception e) {
            Log.e(TAG, "onDelete: " + e);
        }
    }
}
