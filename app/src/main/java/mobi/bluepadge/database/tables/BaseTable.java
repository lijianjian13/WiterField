package mobi.bluepadge.database.tables;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by bluepadge on 16/12/10.
 */

public interface BaseTable<T> {
    String createTableSql();

    String deleteTableSql();

    ContentValues toContentValues(T t);

    T parseCursor(Cursor cursor);
}
