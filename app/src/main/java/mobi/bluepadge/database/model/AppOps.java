package mobi.bluepadge.database.model;

import android.Manifest;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mobi.bluepadge.Application;
import mobi.bluepadge.remoteadblibrary.Shell;
import mobi.bluepadge.ui.Settings;

public class AppOps {

    public static final int MODE_ALLOWED = 0;

    public static final int MODE_IGNORED = 1;

    public static final int MODE_ERRORED = 2;

    public static final int MODE_DEFAULT = 3;

    /**
     * No operation specified.
     */
    public static final int OP_NONE = -1;
    /**
     * Access to coarse location information.
     */
    public static final int OP_COARSE_LOCATION = 0;
    /**
     * Access to fine location information.
     */
    public static final int OP_FINE_LOCATION = 1;
    /**
     * Causing GPS to run.
     */
    public static final int OP_GPS = 2;
    /** */
    public static final int OP_VIBRATE = 3;
    /** */
    public static final int OP_READ_CONTACTS = 4;
    /** */
    public static final int OP_WRITE_CONTACTS = 5;
    /** */
    public static final int OP_READ_CALL_LOG = 6;
    /** */
    public static final int OP_WRITE_CALL_LOG = 7;
    /** */
    public static final int OP_READ_CALENDAR = 8;
    /** */
    public static final int OP_WRITE_CALENDAR = 9;
    /** */
    public static final int OP_WIFI_SCAN = 10;
    /** */
    public static final int OP_POST_NOTIFICATION = 11;
    /** */
    public static final int OP_NEIGHBORING_CELLS = 12;
    /** */
    public static final int OP_CALL_PHONE = 13;
    /** */
    public static final int OP_READ_SMS = 14;
    /** */
    public static final int OP_WRITE_SMS = 15;
    /** */
    public static final int OP_RECEIVE_SMS = 16;
    /** */
    public static final int OP_RECEIVE_EMERGECY_SMS = 17;
    /** */
    public static final int OP_RECEIVE_MMS = 18;
    /** */
    public static final int OP_RECEIVE_WAP_PUSH = 19;
    /** */
    public static final int OP_SEND_SMS = 20;
    /** */
    public static final int OP_READ_ICC_SMS = 21;
    /** */
    public static final int OP_WRITE_ICC_SMS = 22;
    /** */
    public static final int OP_WRITE_SETTINGS = 23;
    /** */
    public static final int OP_SYSTEM_ALERT_WINDOW = 24;
    /** */
    public static final int OP_ACCESS_NOTIFICATIONS = 25;
    /** */
    public static final int OP_CAMERA = 26;
    /** */
    public static final int OP_RECORD_AUDIO = 27;
    /** */
    public static final int OP_PLAY_AUDIO = 28;
    /** */
    public static final int OP_READ_CLIPBOARD = 29;
    /** */
    public static final int OP_WRITE_CLIPBOARD = 30;
    /** */
    public static final int OP_TAKE_MEDIA_BUTTONS = 31;
    /** */
    public static final int OP_TAKE_AUDIO_FOCUS = 32;
    /** */
    public static final int OP_AUDIO_MASTER_VOLUME = 33;
    /** */
    public static final int OP_AUDIO_VOICE_VOLUME = 34;
    /** */
    public static final int OP_AUDIO_RING_VOLUME = 35;
    /** */
    public static final int OP_AUDIO_MEDIA_VOLUME = 36;
    /** */
    public static final int OP_AUDIO_ALARM_VOLUME = 37;
    /** */
    public static final int OP_AUDIO_NOTIFICATION_VOLUME = 38;
    /** */
    public static final int OP_AUDIO_BLUETOOTH_VOLUME = 39;
    /** */
    public static final int OP_WAKE_LOCK = 40;
    /**
     * Continually monitoring location data.
     */
    public static final int OP_MONITOR_LOCATION = 41;
    /**
     * Continually monitoring location data with a relatively high power request.
     */
    public static final int OP_MONITOR_HIGH_POWER_LOCATION = 42;
    /**
     * Retrieve current usage stats via {@link android.app.usage.UsageStatsManager}.
     */
    public static final int OP_GET_USAGE_STATS = 43;
    /** */
    public static final int OP_MUTE_MICROPHONE = 44;
    /** */
    public static final int OP_TOAST_WINDOW = 45;
    /**
     * Capture the device's display contents and/or audio
     */
    public static final int OP_PROJECT_MEDIA = 46;
    /**
     * Activate a VPN connection without user intervention.
     */
    public static final int OP_ACTIVATE_VPN = 47;
    /**
     * Access the WallpaperManagerAPI to write wallpapers.
     */
    public static final int OP_WRITE_WALLPAPER = 48;
    /**
     * Received the assist structure from an app.
     */
    public static final int OP_ASSIST_STRUCTURE = 49;
    /**
     * Received a screenshot from assist.
     */
    public static final int OP_ASSIST_SCREENSHOT = 50;
    /**
     * Read the phone state.
     */
    public static final int OP_READ_PHONE_STATE = 51;
    /**
     * Add voicemail messages to the voicemail content provider.
     */
    public static final int OP_ADD_VOICEMAIL = 52;
    /**
     * Access APIs for SIP calling over VOIP or WiFi.
     */
    public static final int OP_USE_SIP = 53;
    /**
     * Intercept outgoing calls.
     */
    public static final int OP_PROCESS_OUTGOING_CALLS = 54;
    /**
     * User the fingerprint API.
     */
    public static final int OP_USE_FINGERPRINT = 55;
    /**
     * Access to body sensors such as heart rate, etc.
     */
    public static final int OP_BODY_SENSORS = 56;
    /**
     * Read previously received cell broadcast messages.
     */
    public static final int OP_READ_CELL_BROADCASTS = 57;
    /**
     * Inject mock location into the system.
     */
    public static final int OP_MOCK_LOCATION = 58;
    /**
     * Read external storage.
     */
    public static final int OP_READ_EXTERNAL_STORAGE = 59;
    /**
     * Write external storage.
     */
    public static final int OP_WRITE_EXTERNAL_STORAGE = 60;
    /**
     * Turned on the screen.
     */
    public static final int OP_TURN_SCREEN_ON = 61;
    /**
     * Get device accounts.
     */
    public static final int OP_GET_ACCOUNTS = 62;
    /**
     * Control whether an application is allowed to run in the background.
     */
    public static final int OP_RUN_IN_BACKGROUND = 63;
    /**
     * This provides a simple name for each operation to be used
     * in debug output.
     */
    public static final String[] sOpNames = new String[]{
            "COARSE_LOCATION",
            "FINE_LOCATION",
            "GPS",
            "VIBRATE",
            "READ_CONTACTS",
            "WRITE_CONTACTS",
            "READ_CALL_LOG",
            "WRITE_CALL_LOG",
            "READ_CALENDAR",
            "WRITE_CALENDAR",
            "WIFI_SCAN",
            "POST_NOTIFICATION",
            "NEIGHBORING_CELLS",
            "CALL_PHONE",
            "READ_SMS",
            "WRITE_SMS",
            "RECEIVE_SMS",
            "RECEIVE_EMERGECY_SMS",
            "RECEIVE_MMS",
            "RECEIVE_WAP_PUSH",
            "SEND_SMS",
            "READ_ICC_SMS",
            "WRITE_ICC_SMS",
            "WRITE_SETTINGS",
            "SYSTEM_ALERT_WINDOW",
            "ACCESS_NOTIFICATIONS",
            "CAMERA",
            "RECORD_AUDIO",
            "PLAY_AUDIO",
            "READ_CLIPBOARD",
            "WRITE_CLIPBOARD",
            "TAKE_MEDIA_BUTTONS",
            "TAKE_AUDIO_FOCUS",
            "AUDIO_MASTER_VOLUME",
            "AUDIO_VOICE_VOLUME",
            "AUDIO_RING_VOLUME",
            "AUDIO_MEDIA_VOLUME",
            "AUDIO_ALARM_VOLUME",
            "AUDIO_NOTIFICATION_VOLUME",
            "AUDIO_BLUETOOTH_VOLUME",
            "WAKE_LOCK",
            "MONITOR_LOCATION",
            "MONITOR_HIGH_POWER_LOCATION",
            "GET_USAGE_STATS",
            "MUTE_MICROPHONE",
            "TOAST_WINDOW",
            "PROJECT_MEDIA",
            "ACTIVATE_VPN",
            "WRITE_WALLPAPER",
            "ASSIST_STRUCTURE",
            "ASSIST_SCREENSHOT",
            "OP_READ_PHONE_STATE",
            "ADD_VOICEMAIL",
            "USE_SIP",
            "PROCESS_OUTGOING_CALLS",
            "USE_FINGERPRINT",
            "BODY_SENSORS",
            "READ_CELL_BROADCASTS",
            "MOCK_LOCATION",
            "READ_EXTERNAL_STORAGE",
            "WRITE_EXTERNAL_STORAGE",
            "TURN_ON_SCREEN",
            "GET_ACCOUNTS",
            "RUN_IN_BACKGROUND",
    };
    public static final String[] sOpMode = new String[]{
            "allow",
            "ignore",
            "deny",
            "default"
    };
    /**
     * This maps each operation to the operation that serves as the
     * switch to determine whether it is allowed.  Generally this is
     * a 1:1 mapping, but for some things (like location) that have
     * multiple low-level operations being tracked that should be
     * presented to the user as one switch then this can be used to
     * make them all controlled by the same single operation.
     */
    private static int[] sOpToSwitch = new int[]{
            OP_COARSE_LOCATION,
            OP_COARSE_LOCATION,
            OP_COARSE_LOCATION,
            OP_VIBRATE,
            OP_READ_CONTACTS,
            OP_WRITE_CONTACTS,
            OP_READ_CALL_LOG,
            OP_WRITE_CALL_LOG,
            OP_READ_CALENDAR,
            OP_WRITE_CALENDAR,
            OP_COARSE_LOCATION,
            OP_POST_NOTIFICATION,
            OP_COARSE_LOCATION,
            OP_CALL_PHONE,
            OP_READ_SMS,
            OP_WRITE_SMS,
            OP_RECEIVE_SMS,
            OP_RECEIVE_SMS,
            OP_RECEIVE_SMS,
            OP_RECEIVE_SMS,
            OP_SEND_SMS,
            OP_READ_SMS,
            OP_WRITE_SMS,
            OP_WRITE_SETTINGS,
            OP_SYSTEM_ALERT_WINDOW,
            OP_ACCESS_NOTIFICATIONS,
            OP_CAMERA,
            OP_RECORD_AUDIO,
            OP_PLAY_AUDIO,
            OP_READ_CLIPBOARD,
            OP_WRITE_CLIPBOARD,
            OP_TAKE_MEDIA_BUTTONS,
            OP_TAKE_AUDIO_FOCUS,
            OP_AUDIO_MASTER_VOLUME,
            OP_AUDIO_VOICE_VOLUME,
            OP_AUDIO_RING_VOLUME,
            OP_AUDIO_MEDIA_VOLUME,
            OP_AUDIO_ALARM_VOLUME,
            OP_AUDIO_NOTIFICATION_VOLUME,
            OP_AUDIO_BLUETOOTH_VOLUME,
            OP_WAKE_LOCK,
            OP_COARSE_LOCATION,
            OP_COARSE_LOCATION,
            OP_GET_USAGE_STATS,
            OP_MUTE_MICROPHONE,
            OP_TOAST_WINDOW,
            OP_PROJECT_MEDIA,
            OP_ACTIVATE_VPN,
            OP_WRITE_WALLPAPER,
            OP_ASSIST_STRUCTURE,
            OP_ASSIST_SCREENSHOT,
            OP_READ_PHONE_STATE,
            OP_ADD_VOICEMAIL,
            OP_USE_SIP,
            OP_PROCESS_OUTGOING_CALLS,
            OP_USE_FINGERPRINT,
            OP_BODY_SENSORS,
            OP_READ_CELL_BROADCASTS,
            OP_MOCK_LOCATION,
            OP_READ_EXTERNAL_STORAGE,
            OP_WRITE_EXTERNAL_STORAGE,
            OP_TURN_SCREEN_ON,
            OP_GET_ACCOUNTS,
            OP_RUN_IN_BACKGROUND,
    };
    /**
     * This optionally maps a permission to an operation.  If there
     * is no permission associated with an operation, it is null.
     */
    private static String[] sOpPerms = new String[]{
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            null,
            android.Manifest.permission.VIBRATE,
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS,
            android.Manifest.permission.READ_CALL_LOG,
            android.Manifest.permission.WRITE_CALL_LOG,
            android.Manifest.permission.READ_CALENDAR,
            android.Manifest.permission.WRITE_CALENDAR,
            android.Manifest.permission.ACCESS_WIFI_STATE,
            null, // no permission required for notifications
            null, // neighboring cells shares the coarse location perm
            android.Manifest.permission.CALL_PHONE,
            android.Manifest.permission.READ_SMS,
            null, // no permission required for writing sms
            android.Manifest.permission.RECEIVE_SMS,
            "android.permission.RECEIVE_EMERGENCY_BROADCAST"/*android.Manifest.permission.RECEIVE_EMERGENCY_BROADCAST*/,
            android.Manifest.permission.RECEIVE_MMS,
            android.Manifest.permission.RECEIVE_WAP_PUSH,
            android.Manifest.permission.SEND_SMS,
            android.Manifest.permission.READ_SMS,
            null, // no permission required for writing icc sms
            android.Manifest.permission.WRITE_SETTINGS,
            android.Manifest.permission.SYSTEM_ALERT_WINDOW,
            "android.permission.ACCESS_NOTIFICATIONS"/*android.Manifest.permission.ACCESS_NOTIFICATIONS*/,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.RECORD_AUDIO,
            null, // no permission for playing audio
            null, // no permission for reading clipboard
            null, // no permission for writing clipboard
            null, // no permission for taking media buttons
            null, // no permission for taking audio focus
            null, // no permission for changing master volume
            null, // no permission for changing voice volume
            null, // no permission for changing ring volume
            null, // no permission for changing media volume
            null, // no permission for changing alarm volume
            null, // no permission for changing notification volume
            null, // no permission for changing bluetooth volume
            android.Manifest.permission.WAKE_LOCK,
            null, // no permission for generic location monitoring
            null, // no permission for high power location monitoring
            "android.permission.PACKAGE_USAGE_STATS"/*android.Manifest.permission.PACKAGE_USAGE_STATS*/,
            null, // no permission for muting/unmuting microphone
            null, // no permission for displaying toasts
            null, // no permission for projecting media
            null, // no permission for activating vpn
            null, // no permission for supporting wallpaper
            null, // no permission for receiving assist structure
            null, // no permission for receiving assist screenshot
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ADD_VOICEMAIL,
            Manifest.permission.USE_SIP,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            "android.permission.USE_FINGERPRINT"/*Manifest.permission.USE_FINGERPRINT*/,
            Manifest.permission.BODY_SENSORS,
            "android.permission.READ_CELL_BROADCASTS"/*Manifest.permission.READ_CELL_BROADCASTS*/,
            null,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            null, // no permission for turning the screen on
            Manifest.permission.GET_ACCOUNTS,
            null, // no permission for running in background
    };

    private static int strOpToOp(String opName) {
        if (opName == null) {
            return -1;
        }

        for (int i = 0; i < sOpNames.length; i++) {
            if (opName.equals(sOpNames[i])) {
                return i;
            }
        }

        return -1;
    }

    private static int strModeToMode(String opMode) {
        if (opMode == null) {
            return -1;
        }

        for (int i = 0; i < sOpMode.length; i++) {
            if (opMode.equals(sOpMode[i])) {
                return i;
            }
        }

        return -1;
    }

    public static OpInfo getPackageOp(String packageName) {
//        if (Shell.SU.available()) {
//        if (Shell.isActive(Application.getInstance())) {
        OpInfo list = new OpInfo(packageName);

        for (String s : Shell.run(Application.getInstance(), "appops get " + packageName)) {
            Log.d("AppOps:", s);
            int nameEnd = s.indexOf(':');
            if (nameEnd == -1) {
                continue;
            }

            int opId = strOpToOp(s.substring(0, nameEnd).trim());
            if (opId == -1) {
                continue;
            }

            int statusEnd = s.indexOf(";");
            int statusId;
            if (statusEnd != -1) {
                statusId = strModeToMode(s.substring(nameEnd + 1, statusEnd).trim());
            } else {
                statusId = strModeToMode(s.substring(nameEnd + 1).trim());
            }

            if (statusId == -1) {
                continue;
            }

            list.add(new OpEntry(opId, statusId));
            Log.d("appOps getPackageOp", s.substring(0, nameEnd));
        }
        return list;
//        }
//        return null;
    }

    public static OpInfo setPackageOp(String packageName, int opId, int opMode) {
        return setPackageOp(packageName, new int[]{opId}, new int[]{opMode});
    }

    public static OpInfo setPackageOp(String packageName, int[] opId, int[] opMode) {
        List<String> commands = new ArrayList<>();
        for (int i = 0; i < opId.length; i++) {
            commands.add(String.format(Locale.ENGLISH, "appops set %s %s %s", packageName, sOpNames[sOpToSwitch[opId[i]]], sOpMode[opMode[i]]));
        }
        String forceStopCmd = String.format(Locale.ENGLISH, "am force-stop %s", packageName);
        commands.add(forceStopCmd);
//        if (Shell.SU.available()) {
//            Shell.SU.run(commands);
//        }
        Shell.run(Application.getInstance(), commands);

        OpInfo info = getPackageOp(packageName);
        if (info == null) {
            throw new RuntimeException("");
        }

        /*OpEntry entry = info.get(sOpNames[opToSwitch]);
        if (entry == null) {
            throw new RuntimeException("");
        }

        if (entry.getMode() != opMode) {
            throw new RuntimeException("");
        }*/

        return info;
    }

    public static OpInfo resetPackageOp(String packageName) {
//        if (Shell.SU.available()) {
//            Shell.SU.run(String.format(Locale.ENGLISH, "appops reset %s", packageName));
        Shell.run(Application.getInstance(), String.format(Locale.ENGLISH, "appops reset %s", packageName));
//        }
//        if (Shell.isActive())

        OpInfo info = getPackageOp(packageName);
        if (info == null) {
            throw new RuntimeException("");
        }

        return info;
    }

    /**
     * Retrieve the permission associated with an operation, or null if there is not one.
     */
    public static String opToPermission(int op) {
        return sOpPerms[op];
    }

    public final static class OpEntry {
        private int id;
        private int mode;

        public OpEntry(int id, int mode) {
            this.id = id;
            this.mode = mode;
        }

        public int getId() {
            return id;
        }

        public int getMode() {
            return mode;
        }
    }

    public final static class OpInfo extends ArrayList<OpEntry> {
        /*public boolean contains(String opName) {

        }*/
        private String mPackageName;

        public OpInfo(String packageName) {
            mPackageName = packageName;
        }

        public OpEntry get(String opName) {
            int op = strOpToOp(opName);
            if (op == -1) {
                return null;
            }

            return getOpIndex(op);
        }

        public OpEntry getOpIndex(int op) {
            for (OpEntry e : this) {
                if (e.id == op) {
                    return e;
                }
            }
            return null;
        }

        public String getPackageName() {
            return mPackageName;
        }

        public boolean contains(int op) {
            for (OpEntry e : this) {
                if (e.id == op) {
                    return true;
                }
            }
            return false;
        }
    }
}
