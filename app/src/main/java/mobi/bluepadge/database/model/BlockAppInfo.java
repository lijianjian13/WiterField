package mobi.bluepadge.database.model;

/**
 * Created by bluepadge on 16/12/10.
 */

public class BlockAppInfo {
    private String packageName;
    private int blockType;

    public BlockAppInfo() {

    }

    public BlockAppInfo(String packageName, int blockType) {
        this.packageName = packageName;
        this.blockType = blockType;
    }

    public int getBlockType() {
        return blockType;
    }

    public void setBlockType(int blockType) {
        this.blockType = blockType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
