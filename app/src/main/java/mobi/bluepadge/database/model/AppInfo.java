package mobi.bluepadge.database.model;

import android.app.ActivityManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

public class AppInfo {

    private String name = "";
    private String packageName = "";
    private String versionName = "";
    private int versionCode = 0;
    private int targetSdkVersion;
    private Drawable icon;
    private String[] permissions;
    private boolean systemPackage;
    private int blockMethod = AmOps.BLOCK_TYPE_NORMAL;
    private Long installTime;
    private boolean running;
    private ActivityManager.RunningServiceInfo runningInfo;

    public AppInfo(String packageName, PackageManager pm) throws PackageManager.NameNotFoundException {
        this(pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS), pm);
    }

    public AppInfo(PackageInfo pi, PackageManager pm) throws PackageManager.NameNotFoundException {
        this(pi.applicationInfo.loadLabel(pm).toString(),
                pi.packageName,
                pi.versionName,
                pi.versionCode,
                pi.applicationInfo.targetSdkVersion,
                pi.applicationInfo.loadIcon(pm),
                pi.requestedPermissions,
                (pm.getApplicationInfo(pi.packageName, 0).flags & ApplicationInfo.FLAG_SYSTEM) != 0,
                pi.firstInstallTime);
    }

    public AppInfo(String name, String packageName, String versionName, int versionCode,
                   int targetSdkVersion, Drawable icon, String[] permissions, boolean systemPackage, Long installTime) {
        this.name = name;
        this.packageName = packageName;
        this.versionName = versionName;
        this.versionCode = versionCode;
        this.targetSdkVersion = targetSdkVersion;
        this.icon = icon;
        this.permissions = permissions;
        this.systemPackage = systemPackage;
        this.installTime = installTime;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getVersionName() {
        return versionName;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public int getTargetSdkVersion() {
        return targetSdkVersion;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public boolean isSystemPackage() {
        return systemPackage;
    }

    public boolean containPermission(String permission) {
        if (permissions == null) {
            return false;
        }

        for (String p : permissions) {
            if (p.equals(permission)) {
                return true;
            }
        }
        return false;
    }

    public int getBlockMethod() {
        return blockMethod;
    }

    public void setBlockMethod(int blockMethod) {
        this.blockMethod = blockMethod;
    }

    public Long getInstallTime() {
        return installTime;
    }

    public void setInstallTime(Long installTime) {
        this.installTime = installTime;
    }

    public boolean getRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public ActivityManager.RunningServiceInfo getRunningInfo() {
        return runningInfo;
    }

    public void setRunningInfo(ActivityManager.RunningServiceInfo runningInfo) {
        this.runningInfo = runningInfo;
    }
}
