package mobi.bluepadge.database.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by bluepadge on 16/12/10.
 */

public class AmOps {

    /**
     * set package inactive
     */
    public static final int BLOCK_TYPE_NORMAL = 2;
    public static final int BLOCK_TYPE_INACTIVE = 1;
    public static final int BLOCK_TYPE_STOP = 0;

    public static List<String> generateCommand(List<BlockAppInfo> appInfos) {
        List<String> commands = new ArrayList<>();
        for (BlockAppInfo app : appInfos) {
            if (app.getBlockType() == BLOCK_TYPE_INACTIVE) {
                commands.add(String.format(Locale.ENGLISH, "am set-inactive %s true", app.getPackageName()));
            } else if (app.getBlockType() == BLOCK_TYPE_STOP) {
                commands.add(String.format(Locale.ENGLISH, "am force-stop %s", app.getPackageName()));
            }
        }
        return commands;
    }

}
