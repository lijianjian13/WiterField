package mobi.bluepadge.database.model;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import mobi.bluepadge.R;
import mobi.bluepadge.utils.Utils;

public class AppOpsWrapper {
    /**
     * Ops cant be set.
     */

    public static final int OP_STORAGE = AppOps.OP_WRITE_EXTERNAL_STORAGE;

    public static final int[] sOpNonSwitch = new int[] {
            // OP_COARSE_LOCATION
            AppOps.OP_FINE_LOCATION,
            AppOps.OP_GPS,
            AppOps.OP_WIFI_SCAN,
            AppOps.OP_NEIGHBORING_CELLS,
            AppOps.OP_MONITOR_LOCATION,
            AppOps.OP_MONITOR_HIGH_POWER_LOCATION,

            // OP_RECEIVE_SMS
            AppOps.OP_RECEIVE_EMERGECY_SMS,
            AppOps.OP_RECEIVE_MMS,
            AppOps.OP_RECEIVE_WAP_PUSH,

            // OP_READ_SMS
            AppOps.OP_READ_ICC_SMS,

            // OP_WRITE_SMS
            AppOps.OP_WRITE_ICC_SMS,
    };

    public static SparseArray<String> GROUP_LABEL = null;
    private static String[] APPOPS_SUMMARIES = null;

    public static class OpInfo implements Iterable<OpEntry> {

        private AppOps.OpInfo mInfo;
        private List<OpEntry> mEntries;

        void init(Context context) {
            GROUP_LABEL = new SparseArray<>();

            PackageManager pm = context.getPackageManager();
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    GROUP_LABEL.put(R.drawable.ic_permission_sensors_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.SENSORS, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_phone_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.PHONE, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_sms_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.SMS, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_contacts_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.CONTACTS, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_camera_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.CAMERA, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_location_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.LOCATION, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_calender_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.CALENDAR, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_mic_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.MICROPHONE, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_storage_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.STORAGE, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    /*GROUP_LABEL.put(R.drawable.ic_permission_fingerprint_24dp,
                            pm.getPermissionGroupInfo(Manifest.permission_group.PHONE, PackageManager.GET_META_DATA).loadLabel(pm).toString());

                    GROUP_LABEL.put(R.drawable.ic_permission_24dp,
                            10);*/
                } else {
                    GROUP_LABEL.put(R.drawable.ic_permission_sensors_24dp, context.getString(R.string.permission_body_sensor));
                    GROUP_LABEL.put(R.drawable.ic_permission_phone_24dp, context.getString(R.string.permission_phone));
                    GROUP_LABEL.put(R.drawable.ic_permission_sms_24dp, context.getString(R.string.permission_sms));
                    GROUP_LABEL.put(R.drawable.ic_permission_contacts_24dp, context.getString(R.string.permission_contacts));
                    GROUP_LABEL.put(R.drawable.ic_permission_camera_24dp, context.getString(R.string.permission_camera));
                    GROUP_LABEL.put(R.drawable.ic_permission_location_24dp, context.getString(R.string.permission_location));
                    GROUP_LABEL.put(R.drawable.ic_permission_calender_24dp, context.getString(R.string.permission_calender));
                    GROUP_LABEL.put(R.drawable.ic_permission_mic_24dp, context.getString(R.string.permission_microphone));
                    GROUP_LABEL.put(R.drawable.ic_permission_storage_24dp, context.getString(R.string.permission_storage));
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            APPOPS_SUMMARIES = context.getResources().getStringArray(R.array.app_ops_summaries);
        }

        private OpInfo(Context context, AppOps.OpInfo info) {
            if (GROUP_LABEL == null) {
                init(context);
            }

            mInfo = info;
            mEntries = new ArrayList<>();
            for (AppOps.OpEntry e : info) {
                if (e.getId() != AppOps.OP_READ_EXTERNAL_STORAGE) {
                    mEntries.add(new OpEntry(context, e));
                }
            }

            Collections.sort(mEntries, new Comparator<OpEntry>() {
                @Override
                public int compare(OpEntry o1, OpEntry o2) {
                    String l1 = GROUP_LABEL.get(o1.getIconRes());
                    String l2 = GROUP_LABEL.get(o2.getIconRes());
                    if (l1 == null) {
                        l1 = "\uFFFF";
                    }
                    if (l2 == null) {
                        l2 = "\uFFFF";
                    }

                    return (l1 + o1.getLabel()).compareTo(l2 + o2.getLabel());
                }
            });
        }

        public String getPackageName() {
            return mInfo.getPackageName();
        }

        public OpEntry get(int location) {
            return mEntries.get(location);
        }

        public int size() {
            return mEntries.size();
        }

        class MyIterator implements Iterator<OpEntry> {
            private int index = 0;

            public boolean hasNext() {
                return index < size();
            }

            public OpEntry next() {
                return get(index++);
            }

            public void remove() {
                throw new UnsupportedOperationException();

            }
        }

        @Override
        public Iterator<OpEntry> iterator() {
            return new MyIterator();
        }
    }

    public static class OpEntry {
        private AppOps.OpEntry mEntry;

        private String mOpName;
        private String mLabel;
        private String mDescription;
        private Drawable mIcon;
        private int mIconRes;

        private OpEntry(Context context, AppOps.OpEntry entry) {
            mEntry = entry;

            mOpName = AppOps.sOpNames[entry.getId()];

            PackageManager pm = context.getPackageManager();
            switch (entry.getId()) {
                case AppOps.OP_COARSE_LOCATION:
                    setPermissionInfo(pm, Utils.getPermissionGroupInfo(context, Manifest.permission_group.LOCATION), context.getString(R.string.permission_location));
                    break;
                case AppOps.OP_WRITE_EXTERNAL_STORAGE:
                case AppOps.OP_READ_EXTERNAL_STORAGE:
                    setPermissionInfo(pm, Utils.getPermissionGroupInfo(context, Manifest.permission_group.STORAGE), context.getString(R.string.permission_storage));
                    break;
                default:
                    setPermissionInfo(pm, Utils.getPermissionInfo(context, AppOps.opToPermission(entry.getId())));
                    break;
            }

            switch (entry.getId()) {
                case AppOps.OP_RUN_IN_BACKGROUND:
                    mDescription = context.getString(R.string.permission_run_in_background_desc);
                    break;
            }

            switch (entry.getId()) {
                case AppOps.OP_COARSE_LOCATION:
                    setIcon(context, R.drawable.ic_permission_location_24dp);
                    break;
                case AppOps.OP_CAMERA:
                    setIcon(context, R.drawable.ic_permission_camera_24dp);
                    break;
                case AppOps.OP_WRITE_EXTERNAL_STORAGE:
                case AppOps.OP_READ_EXTERNAL_STORAGE:
                    setIcon(context, R.drawable.ic_permission_storage_24dp);
                    break;
                case AppOps.OP_READ_CONTACTS:
                case AppOps.OP_WRITE_CONTACTS:
                case AppOps.OP_GET_ACCOUNTS:
                    setIcon(context, R.drawable.ic_permission_contacts_24dp);
                    break;
                case AppOps.OP_READ_CALL_LOG:
                case AppOps.OP_WRITE_CALL_LOG:
                case AppOps.OP_CALL_PHONE:
                case AppOps.OP_PROCESS_OUTGOING_CALLS:
                case AppOps.OP_READ_PHONE_STATE:
                    setIcon(context, R.drawable.ic_permission_phone_24dp);
                    break;
                case AppOps.OP_READ_CALENDAR:
                case AppOps.OP_WRITE_CALENDAR:
                    setIcon(context, R.drawable.ic_permission_calender_24dp);
                    break;
                case AppOps.OP_READ_SMS:
                case AppOps.OP_WRITE_SMS:
                case AppOps.OP_RECEIVE_SMS:
                case AppOps.OP_SEND_SMS:
                    setIcon(context, R.drawable.ic_permission_sms_24dp);
                    break;
                case AppOps.OP_RECORD_AUDIO:
                case AppOps.OP_MUTE_MICROPHONE:
                    setIcon(context, R.drawable.ic_permission_mic_24dp);
                    break;
                case AppOps.OP_BODY_SENSORS:
                    setIcon(context, R.drawable.ic_permission_sensors_24dp);
                    break;
                /*case AppOps.OP_USE_FINGERPRINT:
                    setIcon(context, R.drawable.ic_permission_fingerprint_24dp);
                    break;*/
                default:
                    setIcon(context, R.drawable.ic_permission_24dp);
                    break;
            }
        }

        void setIcon(Context context, int icon) {
            mIconRes = icon;
            mIcon = context.getDrawable(icon);
        }

        void setPermissionInfo(PackageManager pm, PermissionGroupInfo pi, String defaultLabel) {
            if (pi != null) {
                mLabel = pi.loadDescription(pm).toString()/*pi.loadLabel(pm).toString()*/;
                mDescription = null/*pi.loadDescription(pm).toString()*/;
            } else {
                mLabel = defaultLabel;
                mDescription = null;
            }
        }

        void setPermissionInfo(PackageManager pm, PermissionInfo pi) {
            if (pi != null) {
                mLabel = String.valueOf(pi.loadLabel(pm));
                mDescription = String.valueOf(pi.loadDescription(pm));
            } else {
                //mLabel = AppOps.sOpNames[mEntry.getId()];
                mLabel = APPOPS_SUMMARIES[mEntry.getId()];
                mDescription = null;
            }
        }

        public OpInfo setOp(Context context, String packageName, int opMode) {
            if (getId() == OP_STORAGE) {
                return setPackageOp(context,
                        packageName,
                        new int[] {getId(), AppOps.OP_READ_EXTERNAL_STORAGE},
                        new int[] {opMode, opMode});
            } else {
                return setPackageOp(context, packageName, getId(), opMode);
            }
        }

        public int getId() {
            return mEntry.getId();
        }

        public int getMode() {
            return mEntry.getMode();
        }

        public Drawable getIcon() {
            return mIcon;
        }

        public String getOpName() {
            return mOpName;
        }

        public String getDescription() {
            return mDescription;
        }

        public String getLabel() {
            return mLabel;
        }

        int getIconRes() {
            return mIconRes;
        }

        public String getGroupLabel() {
            String label = GROUP_LABEL.get(mIconRes);
            if (label == null) {
                return "";
            } else {
                return label;
            }
        }
    }

    /**
     * 留下真正可以 appops set 的
     * 当没有存储权限时去掉 OP_WRITE_EXTERNAL_STORAGE 和 OP_READ_EXTERNAL_STORAGE
     */
    public static AppOps.OpInfo filter(Context context, AppOps.OpInfo info) {
        if (info == null) {
            return null;
        }
        AppInfo ai;
        try {
            ai = new AppInfo(info.getPackageName(), context.getPackageManager());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        boolean storage = ai.containPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                && ai.containPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        AppOps.OpInfo newInfo = new AppOps.OpInfo(info.getPackageName());

        for (AppOps.OpEntry e : info) {
            boolean add = true;

            if (e.getId() == AppOps.OP_WRITE_EXTERNAL_STORAGE || e.getId() == AppOps.OP_READ_EXTERNAL_STORAGE) {
                add = storage;
            } else {
                for (int i : sOpNonSwitch) {
                    if (e.getId() == i) {
                        add = false;
                        break;
                    }
                }
            }

            if (add) {
                newInfo.add(e);
            }
        }

        return newInfo;
    }

    public static OpInfo wrap(Context context, AppOps.OpInfo info) {
        info = filter(context, info);
        if (info != null) {
            return new OpInfo(context, filter(context, info));
        } else {
            return null;
        }
    }

    public static OpInfo getPackageOp(Context context, String packageName) {
        return wrap(context, AppOps.getPackageOp(packageName));
    }

    public static OpInfo setPackageOp(Context context, String packageName, int opId, int opMode) {
        return wrap(context, AppOps.setPackageOp(packageName, opId, opMode));
    }

    public static OpInfo setPackageOp(Context context, String packageName, int[] opId, int[] opMode) {
        return wrap(context, AppOps.setPackageOp(packageName, opId, opMode));
    }

    public static OpInfo resetPackageOp(Context context, String packageName) {
        return wrap(context, AppOps.resetPackageOp(packageName));
    }
}
