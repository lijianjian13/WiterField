package mobi.bluepadge;

import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.tencent.bugly.crashreport.CrashReport;

import mobi.bluepadge.ui.Settings;


public class Application extends android.app.Application {
    private static Context sContext;

    public static Context getInstance() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;

//        Stetho.initializeWithDefaults(this);

        Settings.init(this);
        CrashReport.initCrashReport(getApplicationContext(), "618f75a7a2", true);
//        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
    }
}
