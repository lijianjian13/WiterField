package mobi.bluepadge.services;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import java.util.List;

import mobi.bluepadge.Application;
import mobi.bluepadge.remoteadblibrary.Shell;

public class DetectService extends AccessibilityService {

    private static final String TAG = DetectService.class.getSimpleName();
    private static DetectService mInstance = null;
    private String mCurrentForegroundPackageName, mLastForegroundPackageName;

    public DetectService() {
    }

    public static DetectService getInstance() {
        if (mInstance == null) {
            synchronized (DetectService.class) {
                if (mInstance == null) {
                    mInstance = new DetectService();
                }
            }
        }
        return mInstance;
    }

    /**
     * 此方法用来判断当前应用的辅助功能服务是否开启
     *
     * @param context
     * @return
     */
    public static boolean isAccessibilitySettingsOn(Context context) {
        int accessibilityEnabled = 0;
        try {
            accessibilityEnabled = Settings.Secure.getInt(context.getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            Log.d(TAG, e.getMessage());
        }

        if (accessibilityEnabled == 1) {
            String services = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (services != null) {
                return services.toLowerCase().contains(context.getPackageName().toLowerCase());
            }
        }
        return false;
    }

    /**
     * 监听窗口焦点,并且获取焦点窗口的包名
     *
     * @param event
     */
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            mCurrentForegroundPackageName = event.getPackageName().toString();
            if (!TextUtils.equals(mCurrentForegroundPackageName, mLastForegroundPackageName)) {
//                    am set-inactive <packageName> true
                if (!TextUtils.equals(mLastForegroundPackageName, this.getPackageName())) {
                    String cmd = String.format("am set-inactive %s true", mLastForegroundPackageName);
                    new Thread(() -> {
                        List<String> res = Shell.run(Application.getInstance(), cmd);
                        if (res != null) {
                            for (String result : res) {
                                Log.d(TAG, result);
                            }
                        }
                    }).start();
                }
//                Shell.run(this, new String[]{cmd});
//                Log.d(TAG, mLastForegroundPackageName + " is going to inactive");
                mLastForegroundPackageName = mCurrentForegroundPackageName;
            }
        }
    }

    @Override
    public void onInterrupt() {
    }

    public String getForegroundPackage() {
        return mCurrentForegroundPackageName;
    }
}