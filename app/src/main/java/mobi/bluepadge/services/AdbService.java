package mobi.bluepadge.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import mobi.bluepadge.database.model.AppOpsWrapper;
import mobi.bluepadge.remoteadblibrary.Shell;
import mobi.bluepadge.ui.Settings;

public class AdbService extends Service {

    private static final String TAG = AdbService.class.getSimpleName();

    public AdbService() {
    }

    @Override
    public void onCreate() {
//        new Thread(this::connectAdb).start();
        new Thread(() -> {
            List<String> res = Shell.run(AdbService.this, "ls");
//            , Settings.instance().getInt(Settings.ADB_PORT,5555));
            if (res == null) {
                Log.d(TAG, "res is null");
            } else {

                for (String result : res) {
                    Log.d(TAG, result);
                }
            }
        }).start();
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (mAdbConnection == null || !mAdbConnection.isConnected()) {
//            new Thread(this::connectAdb).start();
//        }
//        return super.onStartCommand(intent, flags, startId);
//    }

    public AppOpsWrapper.OpInfo getAppOpsInfo(String packageName) {
        Log.d(TAG, packageName + " is getting");

        return null;
    }

    public class MyBinder extends Binder {
        public AdbService getService() {
            Log.d(TAG, "getService");
            return AdbService.this;
        }
    }
}
