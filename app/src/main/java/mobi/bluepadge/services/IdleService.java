package mobi.bluepadge.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import mobi.bluepadge.R;
import mobi.bluepadge.database.AppDataSource;
import mobi.bluepadge.database.model.AmOps;
import mobi.bluepadge.database.model.BlockAppInfo;
import mobi.bluepadge.remoteadblibrary.Shell;
import mobi.bluepadge.ui.MainActivity;
import mobi.bluepadge.ui.Settings;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;

public class IdleService extends Service {

    public static final String BROADCAST_UNIQUE_KEY = "mobi.bluepadge.idleservice.mDelay";
    public static final String DELAY_KEY = "DELAY_KEY";

    private static final int NOTIFICATION_ID = 0x1;

    private static final String TAG = IdleService.class.getSimpleName();

    private Context mContext;
    private PendingIntent pendingIntent;
    private Intent mIntent;
    private NotificationCompat.Builder mBuilder;
    private int mDelay;
//    private NotificationManager mNotificationManager;

    private Subscription mSubscription;
//    private

    public IdleService() {
    }

    @Override
    public void onCreate() {
        mContext = this;
        mDelay = Settings.instance().getInt(Settings.TIMER, 6);
        startNotification();
        registerScreenStatus();
        super.onCreate();
    }

    private BroadcastReceiver mScreenOReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(BROADCAST_UNIQUE_KEY)) {
                Log.d(TAG, "Got mDelay");
                mDelay = intent.getIntExtra(DELAY_KEY, 6);
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
//                System.out.println("—— SCREEN_ON ——");
                Log.d(TAG, "SCREEN ON");
                inactiveApps(false);
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                Log.d(TAG, "SCREEN OFF");
                inactiveApps(true);
            }
        }

    };

    private void inactiveApps(boolean setInactive) {
//                Observable.timer(3, TimeUnit.MINUTES)
        if (setInactive) {
            AppDataSource dataSource = new AppDataSource(this);
            mSubscription = dataSource.getBlockApps()
                    .delay(mDelay, TimeUnit.MINUTES)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .retry(3)
                    .subscribe(new Subscriber<List<BlockAppInfo>>() {
                        @Override
                        public void onCompleted() {
                            mSubscription.unsubscribe();
                            mSubscription = null;
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());
                        }

                        @Override
                        public void onNext(List<BlockAppInfo> appInfos) {
                            List<String> commands = AmOps.generateCommand(appInfos);
                            Shell.run(getApplicationContext(), commands);
//                                    , Settings.instance().getInt(Settings.ADB_PORT, 5555));
                            Log.d(TAG, "休眠操作已完成");
                        }
                    });
        } else {
            if (mSubscription != null) {
                mSubscription.unsubscribe();
                mSubscription = null;
                Log.d(TAG, "休眠任务已被取消");
            }
        }

    }

    private void registerScreenStatus() {
         /* 注册屏幕唤醒时的广播 */
        IntentFilter mScreenOnFilter = new IntentFilter("android.intent.action.SCREEN_ON");
        registerReceiver(mScreenOReceiver, mScreenOnFilter);

        IntentFilter delayChangeFilter = new IntentFilter(BROADCAST_UNIQUE_KEY);
        registerReceiver(mScreenOReceiver, delayChangeFilter);

        /* 注册机器锁屏时的广播 */
        IntentFilter mScreenOffFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        registerReceiver(mScreenOReceiver, mScreenOffFilter);
    }

    private void startNotification() {
//        status = getAppStatus() ? "前台" : "后台";
        mIntent = new Intent(mContext, MainActivity.class);
        pendingIntent = PendingIntent.getActivity(mContext, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_service_icon)
                .setShowWhen(false)
                .setContentText("Winter Field正在管理应用")
                .setContentTitle("Winter Filed")
                .setPriority(Notification.PRIORITY_MIN)
                .setContentIntent(pendingIntent);
        Notification notification = mBuilder.build();
        startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mScreenOReceiver);
        super.onDestroy();
    }
}
