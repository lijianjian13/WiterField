package mobi.bluepadge.insideadb;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mobi.bluepadge.R;

public class AbdInsideTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abd_inside_test);
    }
}
