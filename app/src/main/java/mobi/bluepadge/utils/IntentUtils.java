package mobi.bluepadge.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.List;


public class IntentUtils {

    public static boolean isValid(Context context, Intent intent) {
        return isValid(context, intent, 0);
    }

    public static boolean isValid(Context context, Intent intent, int minSize) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        return activities.size() > minSize;
    }
}
