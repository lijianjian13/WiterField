package mobi.bluepadge.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;


public class Utils {

    public static PermissionInfo getPermissionInfo(Context context, String permission) {
        if (permission != null) {
            try {
                return context.getPackageManager().getPermissionInfo(permission, PackageManager.GET_META_DATA);
            } catch (PackageManager.NameNotFoundException ignored) {
            }
        }
        return null;
    }

    public static PermissionGroupInfo getPermissionGroupInfo(Context context, String permission) {
        if (permission != null) {
            try {
                return context.getPackageManager().getPermissionGroupInfo(permission, PackageManager.GET_META_DATA);
            } catch (PackageManager.NameNotFoundException ignored) {
            }
        }
        return null;
    }
}
